//
//  ZJDownloadManagerVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-10-24.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJDownloadManagerVC.h"
#import "FilesDownManage.h"
#import "ZJMoviePlayerViewController.h"
#import "ZJPhotoBrowserVC.h"
#import "MBProgressHUD+Add.h"
#import <Social/Social.h>
@interface ZJDownloadManagerVC ()

@property (nonatomic, strong) NSMutableArray *btns;
@end

@implementation ZJDownloadManagerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    // self.navigationController.navigationBar.hidden = YES;
    FilesDownManage *filedownmanage = [FilesDownManage sharedFilesDownManage];
    
    [filedownmanage startLoad];
    self.downingList=filedownmanage.downinglist;
    [self.downloadingTable reloadData];
    
    self.finishedList=filedownmanage.finishedlist;
    [self.finishedTable reloadData];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    // self.navigationController.navigationBar.hidden = NO;
    FilesDownManage *filedownmanage = [FilesDownManage sharedFilesDownManage];
    [filedownmanage saveFinishedFile];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = BACKGROUNDCOLOR;
    
    if (IOS_VERSION >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    
    [FilesDownManage sharedFilesDownManage].downloadDelegate = self;

    [self uploadNav];
    [self uploadTableView];
    [self uploadTopView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)uploadNav
{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40);
    [backButton setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(clickBackButton) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.title = ZJLocalizedString(@"Download Manager");
}

- (void)uploadTopView
{
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kNavbarHeight)];
    topView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    topView.autoresizesSubviews = YES;
    topView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:topView];
    
    for (int i = 0; i < 3; i++) {
        UIView *line = [[UIView alloc] init];
        line.backgroundColor = RGBACOLOR(54, 54, 54, 1);
        
        if (i == 0) {
            line.frame = CGRectMake(0, 0, topView.frame.size.width, 1);
            line.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        }else if (i == 2){
            line.frame = CGRectMake(topView.frame.size.width/2, 12, 1, 20)
            ;
            line.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        }else{
            line.frame = CGRectMake(0, topView.frame.size.height-1, topView.frame.size.width, 1);
            line.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin;
        }
        [topView addSubview:line];
    }
    
    self.btns = [NSMutableArray arrayWithCapacity:2];
    
    for (int i = 0; i < 2; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(self.view.frame.size.width/2*i, 0, self.view.frame.size.width/2, kNavbarHeight);
        button.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        if (i == 0) {
            [button setTitle:ZJLocalizedString(@"Downloading") forState:UIControlStateNormal];
            button.enabled = NO;
        }else{
            [button setTitle:ZJLocalizedString(@"Downloaded") forState:UIControlStateNormal];
        }
        button.tag = i;
        [button setBackgroundImage:[UIImage imageNamed:@"shadow"] forState:UIControlStateDisabled];
        [button setBackgroundImage:nil forState:UIControlStateNormal];
        [button addTarget:self action:@selector(clickModeButton:) forControlEvents:UIControlEventTouchDown];
        [button setTitleColor:RGBACOLOR(204, 204, 204, 1) forState:UIControlStateDisabled];
        [button setTitleColor:RGBACOLOR(204, 204, 204, 0.5) forState:UIControlStateNormal];
        
        [topView addSubview:button];
        
        [self.btns addObject:button];
    }
    
    [self clickModeButton:[self.btns objectAtIndex:0]];
}

- (void)uploadTableView
{
    self.downloadingTable = [[UITableView alloc] initWithFrame:CGRectMake(0, kNavbarHeight, self.view.frame.size.width, self.view.frame.size.height-kNavbarHeight) style:UITableViewStylePlain];
    self.downloadingTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.downloadingTable.backgroundColor = [UIColor clearColor];
    self.downloadingTable.delegate = self;
    self.downloadingTable.dataSource = self;
    self.downloadingTable.separatorColor = RGBACOLOR(60, 60, 60, 1);
    if ([self.downloadingTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.downloadingTable setSeparatorInset:UIEdgeInsetsZero];
    }
    UIView *tableFooterView = [[UIView alloc] init];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.downloadingTable.tableFooterView = tableFooterView;
    
    [self.view addSubview:self.downloadingTable];
    
    self.finishedTable = [[UITableView alloc] initWithFrame:CGRectMake(0, kNavbarHeight, self.view.frame.size.width, self.view.frame.size.height-kNavbarHeight) style:UITableViewStylePlain];
    self.finishedTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.finishedTable.backgroundColor = [UIColor clearColor];
    self.finishedTable.delegate = self;
    self.finishedTable.dataSource = self;
    self.finishedTable.separatorColor = RGBACOLOR(60, 60, 60, 1);
    if ([self.finishedTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.finishedTable setSeparatorInset:UIEdgeInsetsZero];
    }
    UIView *tableFooterView2 = [[UIView alloc] init];
    tableFooterView2.backgroundColor = [UIColor clearColor];
    self.finishedTable.tableFooterView = tableFooterView2;
    [self.view addSubview:self.finishedTable];
}


#pragma mark - Action

- (void)clickBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickModeButton:(UIButton *)button
{
    [self dimAllButtonExceptButton:button];
    
    if (button.tag == 0) {
        self.downloadingTable.hidden = NO;
        self.finishedTable.hidden = YES;
    }else{
        self.downloadingTable.hidden = YES;
        self.finishedTable.hidden = NO;
    }
}

- (void)dimAllButtonExceptButton:(UIButton *)selectedButton
{
    for (int i = 0; i < self.btns.count; i++) {
        UIButton *button = (UIButton *)[self.btns objectAtIndex:i];
        if (button == selectedButton) {
            button.enabled = NO;
        }else{
            button.enabled = YES;
        }
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [[FilesDownManage sharedFilesDownManage]  deleteFinishFile:currentFileModel];
        
        self.finishedList = [FilesDownManage sharedFilesDownManage].finishedlist;
        [self.finishedTable reloadData];
    }
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.downloadingTable)
    {
        return [self.downingList count];
    }
    else
    {
        return [self.finishedList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.downloadingTable)//正在下载的文件列表
    {
        static NSString *downCellIdentifier=@"DownloadCell";
        DownloadCell *cell=(DownloadCell *)[tableView dequeueReusableCellWithIdentifier:downCellIdentifier];
        if(cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DownloadCell" owner:nil options:nil];
            cell = [nib objectAtIndex:0];
            
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.delegate = self;
            [cell.progress1 setTrackImage:[UIImage imageNamed:@"下载管理进度条背景.png"]];
            [cell.progress1 setProgressImage:[UIImage imageNamed:@"下载管理进度背景点九.png"]];
        }
        
        ASIHTTPRequest *theRequest=[self.downingList objectAtIndex:indexPath.row];
 
        FileModel *fileInfo=[theRequest.userInfo objectForKey:@"File"];
        NSString *currentsize = [CommonHelper getFileSizeString:fileInfo.fileReceivedSize];
        NSString *totalsize = [CommonHelper getFileSizeString:fileInfo.fileSize];
        cell.fileName.text=fileInfo.fileName;
        if ([totalsize longLongValue]<=0) {
            cell.fileSize.text = [NSString stringWithFormat:@"%@/%@",currentsize,@"未知"];
        }else
        {
            cell.fileSize.text = [NSString stringWithFormat:@"%@/%@",currentsize,totalsize];
        }
        cell.fileInfo=fileInfo;
        cell.request=theRequest;
        cell.timelable.text =[NSString stringWithFormat:@"%@",fileInfo.time] ;
        cell.timelable.hidden = YES;        
        if ([currentsize longLongValue]==0) {
            [cell.progress1 setProgress:0.0f];
        }else
            [cell.progress1 setProgress:[CommonHelper getProgress:[fileInfo.fileSize longLongValue] currentSize:[fileInfo.fileReceivedSize longLongValue]]];
        cell.sizeinfoLab.text =[NSString stringWithFormat:@"%0.0f%@",100*(cell.progress1.progress),@"%"];
        // NSLog(@"process:%@",cell.sizeinfoLab.text);
        if(fileInfo.isDownloading)//文件正在下载
        {
            [cell.operateButton setBackgroundImage:[UIImage imageNamed:@"下载管理-暂停按钮.png"] forState:UIControlStateNormal];
        }
        else if(!fileInfo.isDownloading && !fileInfo.willDownloading&&!fileInfo.error)
        {
            [cell.operateButton setBackgroundImage:[UIImage imageNamed:@"下载管理-开始按钮.png"] forState:UIControlStateNormal];
            cell.sizeinfoLab.text = @"暂停";
        }else if(!fileInfo.isDownloading && fileInfo.willDownloading&&!fileInfo.error)
        {
            [cell.operateButton setBackgroundImage:[UIImage imageNamed:@"下载管理-开始按钮.png"] forState:UIControlStateNormal];
            cell.sizeinfoLab.text = @"等待";
        }else if (fileInfo.error)
        {
            [cell.operateButton setBackgroundImage:[UIImage imageNamed:@"下载管理-开始按钮.png"] forState:UIControlStateNormal];
            cell.sizeinfoLab.text = @"错误";
        }
      
        return cell;
        
    }else if(tableView==self.finishedTable)//已完成下载的列表
    {
        static NSString *finishedCellIdentifier=@"FinishedCell";
        FinishedCell *cell=(FinishedCell *)[self.finishedTable dequeueReusableCellWithIdentifier:finishedCellIdentifier];
        if(cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FinishedCell" owner:nil options:nil];
            cell = [nib objectAtIndex:0];
            
            cell.backgroundColor = [UIColor clearColor];
            
            cell.delegate = self;
        }
        
        FileModel *fileInfo=[self.finishedList objectAtIndex:indexPath.row];
        cell.fileName.text=fileInfo.fileName;
        
        cell.fileSize.text=[NSString stringWithFormat:@"SIZE:%@",[CommonHelper getFileSizeString:fileInfo.fileSize]];
        cell.fileInfo=fileInfo;
        cell.shareButton.tag=indexPath.row;
        cell.timelable.text =[NSString stringWithFormat:@"DATE:%@",[NSString stringWithFormat:@"%@",fileInfo.time]] ;
        [cell.shareButton addTarget:self action:@selector(shareBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.downloadingTable){
        return 53;
    }else{
        return 45;
    }
}

-(void)updateCellOnMainThread:(FileModel *)fileInfo
{
    NSArray* cellArr = [self.downloadingTable visibleCells];
    
    for(id obj in cellArr)
    {
        if([obj isKindOfClass:[DownloadCell class]])
        {
            DownloadCell *cell=(DownloadCell *)obj;
            if([cell.fileInfo.fileURL isEqualToString: fileInfo.fileURL] && [cell.fileInfo.fileName isEqualToString:fileInfo.fileName])
            {
                NSString *currentsize;
                if (fileInfo.post) {
                    currentsize = fileInfo.fileUploadSize;
                }else
                    currentsize = fileInfo.fileReceivedSize;
                NSString *totalsize = [CommonHelper getFileSizeString:fileInfo.fileSize];
                cell.fileSize.text = [NSString stringWithFormat:@"%@/%@",[CommonHelper getFileSizeString:currentsize],totalsize];
                [cell.progress1 setProgress:[CommonHelper getProgress:[fileInfo.fileSize floatValue] currentSize:[currentsize floatValue]]];
                NSLog(@"%f",cell.progress1 .progress);
                
                cell.sizeinfoLab.text =[NSString stringWithFormat:@"%.0f%@",100*(cell.progress1.progress),@"%"];
                if(fileInfo.isDownloading)//文件正在下载
                {
                    [cell.operateButton setBackgroundImage:[UIImage imageNamed:@"下载管理-暂停按钮.png"] forState:UIControlStateNormal];
                }
                else if(!fileInfo.isDownloading && !fileInfo.willDownloading&&!fileInfo.error)
                {
                    [cell.operateButton setBackgroundImage:[UIImage imageNamed:@"下载管理-开始按钮.png"] forState:UIControlStateNormal];
                    cell.sizeinfoLab.text = @"暂停";
                }else if(!fileInfo.isDownloading && fileInfo.willDownloading&&!fileInfo.error)
                {
                    [cell.operateButton setBackgroundImage:[UIImage imageNamed:@"下载管理-开始按钮.png"] forState:UIControlStateNormal];
                    cell.sizeinfoLab.text = @"等待";
                }else if (fileInfo.error)
                {
                    [cell.operateButton setBackgroundImage:[UIImage imageNamed:@"下载管理-开始按钮.png"] forState:UIControlStateNormal];
                    cell.sizeinfoLab.text = @"错误";
                }
            }
        }
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == self.finishedTable) {
        
        FileModel *fileInfo=[self.finishedList objectAtIndex:indexPath.row];

        if ([fileInfo.fileType isEqualToString:@"mp4"]) {

            ZJMoviePlayerViewController *playerViewController =[[ZJMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:fileInfo.targetPath]];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(myMovieFinishedCallback:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:playerViewController.moviePlayer];
            [playerViewController.moviePlayer play];
            [self presentModalViewController:playerViewController animated:NO];
        }else{
        
            ZJPhotoBrowserVC *browser = [[ZJPhotoBrowserVC alloc] init];
            browser.photos = [NSArray arrayWithObject:[NSURL fileURLWithPath:fileInfo.targetPath]];
            browser.currentPhotoIndex = 0;
            [self presentViewController:browser animated:NO completion:nil];
        }
    }
}

-(void)myMovieFinishedCallback:(NSNotification*)notification
{
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ZJLocalizedString(@"Hint") message:ZJLocalizedString(@"Play ends") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - DownloadDelegate

-(void)startDownload:(ASIHTTPRequest *)request;
{
    NSLog(@"-------开始下载!");
}

-(void)updateCellProgress:(ASIHTTPRequest *)request;
{
    FileModel *fileInfo=[request.userInfo objectForKey:@"File"];
    [self performSelectorOnMainThread:@selector(updateCellOnMainThread:) withObject:fileInfo waitUntilDone:YES];
}

-(void)finishedDownload:(ASIHTTPRequest *)request;
{
    // [self.downingList removeObject:request];
    [self.downloadingTable reloadData];
    [self.finishedTable reloadData];
}

- (void)deleteFinishedFile:(FileModel *)selectFile
{
    currentFileModel = selectFile;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ZJLocalizedString(@"Hint") message:[NSString stringWithFormat:@"%@\"%@\"?",ZJLocalizedString(@"Are you sure to delete"),selectFile.fileName] delegate:self cancelButtonTitle:nil otherButtonTitles:ZJLocalizedString(@"YES"),ZJLocalizedString(@"NO"), nil];
    [alert show];
}

-(void)ReloadDownLoadingTable
{
    self.downingList =[FilesDownManage sharedFilesDownManage].downinglist;
    [self.downloadingTable reloadData];
}
- (void)shareBtnPressed:(UIButton *)btn {
    FileModel *fileInfo=[self.finishedList objectAtIndex:btn.tag];
    NSLog(@"fileInfo=%@",fileInfo);
    NSArray *excludedActivities = @[ UIActivityTypeCopyToPasteboard,UIActivityTypeAirDrop, UIActivityTypePrint,UIActivityTypeSaveToCameraRoll, UIActivityTypeAssignToContact];
    
    
    NSString *message = @"Click Pro";
    if ([fileInfo.fileType isEqualToString:@"mp4"]) {
        NSURL *videoURL = [NSURL fileURLWithPath:fileInfo.targetPath];
        NSArray *arrayOfActivityItems = [NSArray arrayWithObjects:videoURL,message ,nil];
        UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:arrayOfActivityItems applicationActivities:nil];
        controller.excludedActivityTypes = excludedActivities;
        
        [controller setCompletionHandler:^(NSString *activityType, BOOL completed) {
            //NSLog(@"completed dialog - activity: %@ - finished flag: %d", activityType, completed);
            NSLog(@"Done");
            
        }];
        
         [self presentViewController:controller animated:YES completion:nil];
       
       
    }else{
        UIImage *img=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL fileURLWithPath:fileInfo.targetPath]]];
        NSArray *arrayOfActivityItems = [NSArray arrayWithObjects:img,message ,nil];
        UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:arrayOfActivityItems applicationActivities:nil];
        controller.excludedActivityTypes = excludedActivities;
        [self presentViewController:controller animated:YES completion:nil];
    }

   NSLog(@"ShareBtnPressed");
    
}


@end
