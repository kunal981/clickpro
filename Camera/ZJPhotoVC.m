//
//  ZJPhotoVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-11-7.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJPhotoVC.h"
#import "ZJPhotoBrowserVC.h"
#import "ZJCameraListCell.h"
#import "FilesDownManage.h"

@interface ZJPhotoVC ()

@end

@implementation ZJPhotoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if (IOS_VERSION >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
        
    [self uploadGripView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)uploadGripView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 0, 300, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = RGBACOLOR(60, 60, 60, 1);
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 12)];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = tableHeaderView;
    
    UIView *tableFooterView = [[UIView alloc] init];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = tableFooterView;
    
    [self.view addSubview:self.tableView];
}

#pragma mark - Action

- (void)clickDownload:(UIButton *)button
{
    NSString *urlStr = [self.photoArr objectAtIndex:button.tag];
    NSString *fileName = [urlStr lastPathComponent];
    NSString *filePath = [fileName pathExtension];
    NSLog(@"fileName:%@ filePath:%@",fileName,filePath);
    [[FilesDownManage sharedFilesDownManage] downFileUrl:urlStr filename:fileName filetarget:filePath fileimage:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.photoArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifity = @"Cell";
    
    ZJCameraListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifity];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ZJCameraListCell" owner:nil options:nil];
        cell = [nib objectAtIndex:0];
        
        cell.backgroundColor = [UIColor clearColor];
        
        [cell.downLoadButton addTarget:self action:@selector(clickDownload:) forControlEvents:UIControlEventTouchDown];
    }

    NSString *imageUrl = [self.photoArr objectAtIndex:indexPath.row];
    cell.mediaNameLabel.text = [imageUrl lastPathComponent];
    cell.downLoadButton.tag = indexPath.row;
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ZJPhotoBrowserVC *browser = [[ZJPhotoBrowserVC alloc] init];
//    browser.photos = self.photoArr;
    browser.currentPhotoIndex = 0;
    browser.photos = [NSArray arrayWithObject:[NSURL URLWithString:[self.photoArr objectAtIndex:indexPath.row]]];
    [self presentViewController:browser animated:NO completion:nil];
}

@end