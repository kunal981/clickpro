//
//  ZJConfigClass.h
//  Camera
//
//  Created by Apple_ZJ on 13-11-29.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZJInterfaceEntry.h"
#import "TFHpple.h"

@protocol ZJConfigDelegate <NSObject>

- (void)getConfigFinished;
- (void)getConfigFinishedError;

@end

@interface ZJConfigClass : NSObject

@property (nonatomic, strong) NSString *videoResolution;
@property (nonatomic, strong) NSMutableArray *videoResolutionArr;

@property (nonatomic, strong) NSString *dualStreams;
@property (nonatomic, strong) NSMutableArray *dualStreamsArr;

@property (nonatomic, strong) NSString *videoStamp;
@property (nonatomic, strong) NSMutableArray *videoStampArr;

@property (nonatomic, strong) NSString *burstMode;
@property (nonatomic, strong) NSMutableArray *burstModeArr;

@property (nonatomic, strong) NSString *photoSize;
@property (nonatomic, strong) NSMutableArray *photoSizeArr;

@property (nonatomic, strong) NSString *photoStamp;
@property (nonatomic, strong) NSMutableArray *photoStampArr;

@property (nonatomic, weak) id <ZJConfigDelegate>delegate;

+ (id)sharedInstance;
- (void)getCofigSetting:(id)dele;
@end
