//
//  FinishedCell.m


#import "FinishedCell.h"
#import "FilesDownManage.h"
#import "ZJDownloadManagerVC.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Social/Social.h>

@implementation FinishedCell

@synthesize fileInfo;
@synthesize fileName,fileSize,timelable;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (IBAction)deleteFile:(id)sender{
    if ([self.delegate respondsToSelector:@selector(deleteFinishedFile:)]) {
        [(ZJDownloadManagerVC*)self.delegate deleteFinishedFile:fileInfo];
    }
}

- (IBAction)openFile:(UIButton *)sender {

}


- (void)dealloc
{
    [fileInfo release];
    [fileSize release];
    [timelable release];
    [fileName release];
    [super dealloc];
}
@end
