//
//  ZJCameraListVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-10-24.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJCameraListVC.h"
#import "ZJInterfaceEntry.h"
#import "TFHpple.h"
#import "MBProgressHUD.h"
#import "ZJDownloadManagerVC.h"
#import "FilesDownManage.h"

#define URL @"http://192.168.42.1/DCIM/100MEDIA/"

#define IMAGENUM 20

@interface ZJCameraListVC ()

@property (nonatomic, strong) NSMutableArray *btns;
@end

@implementation ZJCameraListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.view.backgroundColor = BACKGROUNDCOLOR;
    
    if (IOS_VERSION >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    
    self.photoArr = [NSMutableArray array];
    self.videoArr = [NSMutableArray array];
    
    [FilesDownManage sharedFilesDownManageWithBasepath:@"DownLoad" TargetPathArr:[NSArray arrayWithObject:@"DownLoad/media"]];

    [self uploadNav];
    [self uploadTopView];
    [self getMedia];
//    [self uploadGripView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)uploadNav
{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40);
    [backButton setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(clickBackButton) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 40, 40);
    [rightButton setImage:[UIImage imageNamed:@"icon_folder"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(clickRightButton) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}

- (void)uploadTopView
{
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kNavbarHeight)];
    topView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    topView.autoresizesSubviews = YES;
    topView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:topView];
    
    for (int i = 0; i < 3; i++) {
        UIView *line = [[UIView alloc] init];
        line.backgroundColor = RGBACOLOR(54, 54, 54, 1);
        
        if (i == 0) {
            line.frame = CGRectMake(0, 0, topView.frame.size.width, 1);
            line.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        }else if (i == 2){
            line.frame = CGRectMake(topView.frame.size.width/2, 12, 1, 20)
            ;
            line.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        }else{
            line.frame = CGRectMake(0, topView.frame.size.height-1, topView.frame.size.width, 1);
            line.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin;
        }
        [topView addSubview:line];
    }
    
    self.btns = [NSMutableArray arrayWithCapacity:2];

    for (int i = 0; i < 2; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(self.view.frame.size.width/2*i, 0, self.view.frame.size.width/2, kNavbarHeight);
        button.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        if (i == 0) {
            [button setTitle:ZJLocalizedString(@"Photograph") forState:UIControlStateNormal];
            button.enabled = NO;
        }else{
            [button setTitle:ZJLocalizedString(@"Video") forState:UIControlStateNormal];
        }
        button.tag = i;
        [button setBackgroundImage:[UIImage imageNamed:@"shadow"] forState:UIControlStateDisabled];
        [button setBackgroundImage:nil forState:UIControlStateNormal];
        [button addTarget:self action:@selector(clickModeButton:) forControlEvents:UIControlEventTouchDown];
        [button setTitleColor:RGBACOLOR(204, 204, 204, 1) forState:UIControlStateDisabled];
        [button setTitleColor:RGBACOLOR(204, 204, 204, 0.5) forState:UIControlStateNormal];
        
        [topView addSubview:button];
        
        [self.btns addObject:button];
    }
}

- (void)uploadGripView
{
    self.contentView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kNavbarHeight, self.view.frame.size.width, self.view.frame.size.height-kNavbarHeight)];
    self.contentView.delegate = self;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    self.contentView.backgroundColor = [UIColor clearColor];
    self.contentView.pagingEnabled = YES;
    [self.view addSubview:self.contentView];
    
    self.contentView.contentSize = CGSizeMake(self.contentView.frame.size.width*2, self.contentView.frame.size.height);
    
    if (self.photoVC == nil) {
        
        self.photoVC = [[ZJPhotoVC alloc] init];
        self.photoVC.photoArr = self.photoArr;
        [self addChildViewController:self.photoVC];
        self.photoVC.view.frame  =  CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
        [self.contentView addSubview:self.photoVC.view];
    }
    
    if (self.videoVC == nil) {
        
        self.videoVC = [[ZJVideoVC alloc] init];
        self.videoVC.videoArr = self.videoArr;
        [self addChildViewController:self.videoVC];
        
        self.videoVC.view.frame  =  CGRectMake(self.contentView.frame.size.width, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
        [self.contentView addSubview:self.videoVC.view];
    }
}

#pragma mark - Action

- (void)clickBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickRightButton
{
    ZJDownloadManagerVC *downloadManagerVC = [[ZJDownloadManagerVC alloc] init];
    [self.navigationController pushViewController:downloadManagerVC animated:YES];
}

- (void)clickModeButton:(UIButton *)button
{
    [self dimAllButtonExceptButton:button];
    
    [self.contentView setContentOffset:CGPointMake(self.contentView.frame.size.width*button.tag, 0) animated:NO];
}

- (void)dimAllButtonExceptButton:(UIButton *)selectedButton
{
    for (int i = 0; i < self.btns.count; i++) {
        UIButton *button = (UIButton *)[self.btns objectAtIndex:i];
        if (button == selectedButton) {
            button.enabled = NO;
        }else{
            button.enabled = YES;
        }
    }
}

- (void)clickButton:(UIButton *)button
{

}

#pragma mark - Data

- (void)getMedia
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(didFinished:);
    interface.didFinishedError = @selector(didFinishedError);
    [interface getMedia];
}

- (void)didFinished:(id)data
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];

    TFHpple *xpathParser = [[TFHpple alloc] initWithHTMLData:data];
    NSArray *elements = [xpathParser searchWithXPathQuery:@"//a[@class='link']"];
    for (TFHppleElement *element in elements) {
        
        for (TFHppleElement *chileElement in element.children) {
            
            if ([chileElement.content.pathExtension isEqualToString:@"JPG"]) {
                [self.photoArr addObject:[NSString stringWithFormat:@"%@%@",URL,chileElement.content]];
            }else{
                [self.videoArr addObject:[NSString stringWithFormat:@"%@%@",URL,chileElement.content]];
            }
        }
    }
    
    [self uploadGripView];
}

- (void)didFinishedError
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];

    [self uploadGripView];
}

#pragma mark -  UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (pageControlUsed) {
        return;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    pageControlUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    pageControlUsed = YES;

    CGFloat pageWidth = self.contentView.frame.size.width;
    int page = floor((self.contentView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page >= 2 || page<0) return;
    
    [self dimAllButtonExceptButton:[self.btns objectAtIndex:page]];
}

@end
