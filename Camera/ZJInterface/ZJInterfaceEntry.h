//
//  ZZInterfaceEntry.h
//  Jcconsulting
//
//  Created by Z J on 13-4-3.
//  Copyright (c) 2013年 Dream. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJInterfaceEntry : NSObject<NSXMLParserDelegate, NSURLConnectionDelegate>
{
    Class _originalClass;
}

@property (nonatomic, weak) id delegate;
@property (nonatomic) SEL didFinished;
@property (nonatomic) SEL didFinishedError;
@property (nonatomic, strong) NSMutableData *returnData;

- (void)connectCamera;
- (void)disConnectCamera;
- (void)openStream;
- (void)setStreamType;
- (void)resetVF;
- (void)takePhoto;
- (void)startRecord;
- (void)stopRecord;
- (void)getMedia;
- (void)formatSD;
- (void)getSetting:(NSString *)tabType;
- (void)readyToConfig;
- (void)setConfigWithParam:(NSString *)param Value:(NSString *)value;
- (void)postURLRequest:(NSString *)service;
@end
