//
//  ZZInterfaceEntry.m
//  Jcconsulting
//
//  Created by Z J on 13-4-3.
//  Copyright (c) 2013年 Dream. All rights reserved.
//

#import "ZJInterfaceEntry.h"

#define MainURL @"http://192.168.42.1"

Class object_getClass(id object);

@implementation ZJInterfaceEntry

- (id)init {
    self = [super init];
    if (self) {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    }
    return self;
}

#pragma mark - 连接相机

- (void)connectCamera
{
    NSString *service = @"/cgi-bin/cgi?CMD=SESSION_START";
    
    [self postURLRequest:service];
}

#pragma mark - 断开连接相机

- (void)disConnectCamera
{
    NSString *service = @"/cgi-bin/cgi?CMD=SESSION_STOP";
    
    [self postURLRequest:service];
}

#pragma mark - 视频流格式

- (void)setStreamType
{
    //none rtsp mjpg hls  默认设置rtsp
    NSString *service = @"/cgi-bin/cgi?stream+typeSETPARAM=rtsp";
    
    [self postURLRequest:service];
}

#pragma mark - 开启视频流

- (void)openStream
{
    //none rtsp mjpg hls  默认设置rtsp
    NSString *service = @"/cgi-bin/cgi?streamingSETPARAM=on";
    
    [self postURLRequest:service];
}

#pragma mark - 刷新实时视频流

- (void)resetVF
{
    NSString *service = @"/cgi-bin/cgi?CMD=RESET_VF";
    
    [self postURLRequest:service];
}

#pragma mark - 拍照

- (void)takePhoto
{
    NSString *service = @"/cgi-bin/cgi?CMD=TAKE_PHOTO";
    
    [self postURLRequest:service];
}

#pragma mark - 开始录像

- (void)startRecord
{
    NSString *service = @"/cgi-bin/cgi?CMD=START_RECORD";
    
    [self postURLRequest:service];
}

#pragma mark - 停止录像

- (void)stopRecord
{
    NSString *service = @"/cgi-bin/cgi?CMD=RESTORE_VIEW";
    
    [self postURLRequest:service];
}

#pragma mark - 媒体文件列表

- (void)getMedia
{
    NSString *service = @"/DCIM/100MEDIA?order=N";
        
    [self postURLRequest:service];
}

#pragma mark - 格式化sd卡

- (void)formatSD
{
    NSString *service = @"/cgi-bin/cgi?CMD=FORMAT_SD";
    
    [self postURLRequest:service];
}

#pragma mark - 获取Setting

- (void)getSetting:(NSString *)tabType
{
    NSString *service = [NSString stringWithFormat:@"/cgi-bin/cgi?SETTINGS=%@",tabType];
    
    [self postURLRequest:service];
}

#pragma mark - 进入设置

- (void)readyToConfig
{
    NSString *service = @"/cgi-bin/cgi?CMD=CAM_SETTINGS";
    
    [self postURLRequest:service];
}

#pragma mark - 设置

- (void)setConfigWithParam:(NSString *)param Value:(NSString *)value
{
    NSString *service = [NSString stringWithFormat:@"/cgi-bin/cgi?%@=%@",param,value];
    
    [self postURLRequest:service];
}

#pragma mark - NSURLConnection
- (void)postURLRequest:(NSString *)service
{
    _originalClass = object_getClass(self.delegate);

    NSString *urlString = [NSString stringWithFormat:@"%@%@", MainURL,service];
    
    NSLog(@"请求URL:%@",urlString);
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:10];
    [request setHTTPMethod:@"GET"];

    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection) {
        self.returnData = nil;
        self.returnData = [NSMutableData data];
        NSLog(@"theConnection Success");
    }else {
        NSLog(@"theConnection Fail!!!!");
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

#pragma mark - NSURLConnectionDataDelegate

// 收到响应时, 会触发
- (void)connection:(NSURLConnection *)aConnection didReceiveResponse:(NSURLResponse *)aResponse
{
    
}

// 每收到一次数据, 会调用一次
- (void)connection:(NSURLConnection *)aConn didReceiveData:(NSData *)data
{
    [self.returnData appendData:data];
}

// 全部数据接收完毕时触发
- (void)connectionDidFinishLoading:(NSURLConnection *)aConn
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//    
    NSString *string = [[NSString alloc] initWithData:self.returnData encoding:NSUTF8StringEncoding];
    NSLog(@"string:%@",string);

    Class currentClass = object_getClass(self.delegate);
   
    if (currentClass == _originalClass) {

        if (self.delegate && [self.delegate respondsToSelector:self.didFinished]) {
            [self.delegate performSelectorOnMainThread:self.didFinished withObject:self.returnData waitUntilDone:YES];
        }
    }
}

// 网络错误时触发
- (void)connection:(NSURLConnection *)aConn didFailWithError:(NSError *)error
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    Class currentClass = object_getClass(self.delegate);
    
    if (currentClass == _originalClass) {
        
        // 如果delegate没有被释放
        if (self.delegate && [self.delegate respondsToSelector:self.didFinished]) {
            if (self.delegate && [self.delegate respondsToSelector:self.didFinishedError]){
                [self.delegate performSelectorOnMainThread:self.didFinishedError withObject:nil waitUntilDone:YES];
            }
        }
    }
}

@end
