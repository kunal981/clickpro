//
//  ZJVideoPlayerVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-11-11.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ZJVideoPlayerVC : UIViewController<UIGestureRecognizerDelegate>
{
    BOOL playWhenReady;
    BOOL scrubBuffering;
}
@property (nonatomic, strong) NSMutableArray *videoArr;
@property (nonatomic) NSInteger currentVideoIndex;

@property (nonatomic) BOOL playerIsBuffering;
@property (nonatomic) BOOL restoreVideoPlayStateAfterScrubbing;
@property (nonatomic) BOOL seekToZeroBeforePlay;

@end
