//
//  ZJPhotoBrowserVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-11-12.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDImageCache.h"
#import <AVFoundation/AVFoundation.h>

@interface ZJPhotoBrowserVC : UIViewController

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic) NSInteger currentPhotoIndex;
@end
