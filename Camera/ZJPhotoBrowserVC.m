//
//  ZJPhotoBrowserVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-11-12.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJPhotoBrowserVC.h"
#import "MBProgressHUD+Add.h"
#import "PZPhotoView.h"

#define PLAYER_CONTROL_BAR_HEIGHT 44
#define TOPVIEW_HEIGHT 44

@interface ZJPhotoBrowserVC ()<PZPhotoViewDelegate, UIScrollViewDelegate>
{
    int kNumberOfPages;
    BOOL pageControlUsed;
}

@property(nonatomic) BOOL isControlViewHidden;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *pageViews;

@property(nonatomic, strong) UIView *topView;
@property(nonatomic, strong) UIButton *closeButton;
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, strong) UIButton *deleteButton;
@property(nonatomic, strong) UIButton *downLoadButton;

@property(nonatomic, strong) UIView *bottomView;
@property(nonatomic, strong) UIButton *previousButton;
@property(nonatomic, strong) UIButton *nextButton;

@end

@implementation ZJPhotoBrowserVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = BACKGROUNDCOLOR;
    
    if (IOS_VERSION >= 7.0) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.pageViews = [NSMutableArray arrayWithCapacity:2];
    
    [self createScrollView];
    
    [self createTopView];
//    [self createBottomView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

#pragma mark - UI

- (void)createScrollView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height+10, self.view.frame.size.width)];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    [self.view addSubview:_scrollView];
    
    UITapGestureRecognizer *singleTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleFingerEvent:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.scrollView addGestureRecognizer:singleTap];
    
    [self uploadPhotoView];
}

- (void)uploadPhotoView{
    
    kNumberOfPages = self.photos.count;
    
    NSMutableArray *views = [[NSMutableArray alloc] init];
    for (int i = 0; i < kNumberOfPages; i++) {
        [views addObject:[NSNull null]];
    }
    self.pageViews = views;
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width*kNumberOfPages, self.scrollView.frame.size.height);
    
    
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width*self.currentPhotoIndex, 0) animated:NO];

    [self refreshScrollViewWithPage:self.currentPhotoIndex];
}

- (void)refreshScrollViewWithPage:(int)page
{
    NSString *title = [[self.photos objectAtIndex:self.currentPhotoIndex] lastPathComponent];
    _titleLabel.text = title;
    
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    [self removeScrollViewWithPage:page-2];
    [self removeScrollViewWithPage:page+2];
}

- (void)loadScrollViewWithPage:(int)page
{
    if (page >= kNumberOfPages || page<0) return;
    
//    ZJPhotoView *imageView = [self.pageViews objectAtIndex:page];
    
    PZPhotoView *imageView = [self.pageViews objectAtIndex:page];

    if ((NSNull *)imageView == [NSNull null]) {
        
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        frame.size.width = frame.size.width-10;
        
        imageView = [[PZPhotoView alloc] initWithFrame:frame];
        imageView.photoViewDelegate = self;
        [imageView displayImage:[self.photos objectAtIndex:page]];
        imageView.backgroundColor = [UIColor lightGrayColor];
        [self.pageViews replaceObjectAtIndex:page withObject:imageView];
    }
    
    // add the view to the scroll view
    if (imageView.superview == nil) {
        [self.scrollView addSubview:imageView];
    }
}

- (void)removeScrollViewWithPage:(int)page
{
    if (page >= kNumberOfPages || page<0) return;
    
    PZPhotoView *photoView = [self.pageViews objectAtIndex:page];
    
    if ((NSNull *)photoView != [NSNull null]) {
        
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
        [photoView prepareForReuse];
        [photoView removeFromSuperview];
        photoView = nil;
    }
}

- (void)createTopView
{
    _topView = [[UIView alloc] init];
    [_topView setFrame:CGRectMake(0, 0, self.view.bounds.size.width, TOPVIEW_HEIGHT+STATUS_HEIGHT)];
    _topView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_topView setOpaque:NO];
    [_topView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    _topView.hidden = YES;
    [self.view addSubview:_topView];
    
    _closeButton = [[UIButton alloc] init];
    [_closeButton setFrame:CGRectMake(5, 2+STATUS_HEIGHT, 40, 40)];
    [_closeButton setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [_closeButton addTarget:self action:@selector(clickCloseButton) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_closeButton];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    NSString *title = [[self.photos objectAtIndex:self.currentPhotoIndex] lastPathComponent];
    _titleLabel.text = title;
    [_titleLabel setFrame:CGRectMake(45,7+STATUS_HEIGHT,self.view.bounds.size.width-90,30)];
    _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_titleLabel setFont:[UIFont fontWithName:@"Forza-Medium" size:13.0f]];
    [_titleLabel setTextColor:RGBACOLOR(254, 254, 254, 1)];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [_topView addSubview:_titleLabel];
    
//    _deleteButton = [[UIButton alloc] init];
//    [_deleteButton setFrame:CGRectMake(self.view.bounds.size.width-75, 7, 30, 30)];
//    [_deleteButton setImage:[UIImage imageNamed:@"btn_delete"] forState:UIControlStateNormal];
//    [_topView addSubview:_deleteButton];
    
//    _downLoadButton = [[UIButton alloc] init];
//    _downLoadButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
//    [_downLoadButton setFrame:CGRectMake(self.view.bounds.size.width-30-10, 7+STATUS_HEIGHT, 30, 30)];
//    [_downLoadButton setImage:[UIImage imageNamed:@"btn_download"] forState:UIControlStateNormal];
//    [_downLoadButton addTarget:self action:@selector(clickDownLoadButton) forControlEvents:UIControlEventTouchUpInside];
//    [_topView addSubview:_downLoadButton];
}

- (void)createBottomView
{
    _bottomView = [[UIView alloc] init];
    _bottomView.hidden = YES;
    [_bottomView setFrame:CGRectMake(0,self.view.bounds.size.height - PLAYER_CONTROL_BAR_HEIGHT,self.view.bounds.size.width,PLAYER_CONTROL_BAR_HEIGHT)];
    _bottomView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    [_bottomView setOpaque:NO];
    [_bottomView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    [self.view addSubview:_bottomView];
    
    _previousButton = [[UIButton alloc] init];
    _previousButton.frame = CGRectMake(80, 2, 40, 40);
    [_previousButton setImage:[UIImage imageNamed:@"left_arrow"] forState:UIControlStateNormal];
    [_previousButton setShowsTouchWhenHighlighted:YES];
    [_previousButton addTarget:self action:@selector(clickPreviousButton) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:_previousButton];
    
    if (self.currentPhotoIndex <= 0) {
        _previousButton.enabled = NO;
    }
    
    _nextButton = [[UIButton alloc] init];
    _nextButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    _nextButton.frame = CGRectMake(self.view.frame.size.width-80-40, 2, 40, 40);
    [_nextButton setImage:[UIImage imageNamed:@"right_arrow"] forState:UIControlStateNormal];
    [_nextButton addTarget:self action:@selector(clickNextButton) forControlEvents:UIControlEventTouchUpInside];
    [_nextButton setShowsTouchWhenHighlighted:YES];
    [_bottomView addSubview:_nextButton];
    
    if (self.currentPhotoIndex >= self.photos.count-1) {
        _nextButton.enabled = NO;
    }
}

#pragma mark - PZPhotoViewDelegate
#pragma mark -

- (void)photoViewDidSingleTap:(PZPhotoView *)photoView {
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.8];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    animation.type = kCATransitionFade;
    self.topView.hidden = !self.topView.hidden;
    self.bottomView.hidden = !self.bottomView.hidden;
    [self.topView.layer addAnimation:animation forKey:nil];
    [self.bottomView.layer addAnimation:animation forKey:nil];
}

- (void)photoViewDidDoubleTap:(PZPhotoView *)photoView {
    // do nothing
}

- (void)photoViewDidTwoFingerTap:(PZPhotoView *)photoView {
    // do nothing
}

- (void)photoViewDidDoubleTwoFingerTap:(PZPhotoView *)photoView {
}


#pragma mark - Action

- (void)handleSingleFingerEvent:(UITapGestureRecognizer *)sender
{
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.8];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    animation.type = kCATransitionFade;
    self.topView.hidden = !self.topView.hidden;
    self.bottomView.hidden = !self.bottomView.hidden;
    [self.topView.layer addAnimation:animation forKey:nil];
    [self.bottomView.layer addAnimation:animation forKey:nil];
}

- (void)clickCloseButton
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)clickDownLoadButton
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[self.photos objectAtIndex:self.currentPhotoIndex]];

        UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    });
}

- (void)clickPreviousButton
{
    self.currentPhotoIndex--;

    if (self.currentPhotoIndex <= 0) {
        self.currentPhotoIndex = 0;
        _previousButton.enabled = NO;
    }
    
    _nextButton.enabled = YES;

    [_scrollView setContentOffset:CGPointMake(self.currentPhotoIndex*self.scrollView.frame.size.width, 0) animated:YES];
    [self refreshScrollViewWithPage:self.currentPhotoIndex];
}

- (void)clickNextButton
{
    self.currentPhotoIndex++;
    
    if (self.currentPhotoIndex >= self.photos.count-1) {
        self.currentPhotoIndex = self.photos.count-1;
        _nextButton.enabled = NO;
    }
    _previousButton.enabled = YES;
    
    [_scrollView setContentOffset:CGPointMake(self.currentPhotoIndex*self.scrollView.frame.size.width, 0) animated:YES];
    [self refreshScrollViewWithPage:self.currentPhotoIndex];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (error) {
        [MBProgressHUD showError:@"存入相册失败！" toView:nil];
    } else {
        [MBProgressHUD showSuccess:@"已存入相册！" toView:nil];
    }
}

#pragma mark -  UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (pageControlUsed) {
        return;
    }
    
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    self.currentPhotoIndex = page;

    [self refreshScrollViewWithPage:page];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    pageControlUsed = YES;
    
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page >= kNumberOfPages || page<0) return;
    
    if (self.photos.count > 1) {
        if (page == 0) {
            _previousButton.enabled = NO;
            _nextButton.enabled = YES;
        }else if (page == self.photos.count-1){
            _nextButton.enabled = NO;
            _previousButton.enabled = YES;
        }else{
            _previousButton.enabled = YES;
            _nextButton.enabled = YES;
        }
    }else{
    
        _previousButton.enabled = NO;
        _nextButton.enabled = NO;
    }
}

@end
