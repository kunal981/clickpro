//
//  ZJDefineCollection.h
//  GuMei
//
//  Created by Apple_ZJ on 13-6-10.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

typedef NS_ENUM(NSInteger, Session){
    Session_Connect = 0,
    Session_Disconnect,
    Session_TakePhoto,
    Session_Continuous,
    Session_TakeVideo_Start,
    Session_TakeVideo_Stop,
    Session_GetVideoResolution,
};

#define ISDEBUG 1
#ifdef ISDEBUG
#else
#define NSLog(...);
#endif

#define VIDEO_PATH [[[NSHomeDirectory()   stringByAppendingPathComponent:@"Library"] stringByAppendingPathComponent:@"Caches"] stringByAppendingPathComponent:@"Video"]

#define HOST @"192.168.42.1"
#define PORT 7878

#define VIDEOTAB @"videoTab"
#define PHOTOTAB @"photoTab"

#define BACKGROUNDCOLOR [UIColor colorWithRed:(0)/255.0f green:(0)/255.0f blue:(0)/255.0f alpha:(1)]

#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]

#define STATUS_HEIGHT ((IOS_VERSION >= 7.0)?20:0)

#define APP_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define ISIPHONE5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define ISPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#define ZJLocalizedString(_localize) NSLocalizedStringFromTable(_localize,@"InfoPlist", nil)

#define kNavbarHeight 44