//
//  ZJPhotoVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-11-7.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJVideoVC.h"
#import "ZJVideoPlayerVC.h"
#import "ZJCameraListCell.h"
#import "UINavigationController+Rotation.h"
#import "ZJMoviePlayerViewController.h"
#include "FilesDownManage.h"

@interface ZJVideoVC ()

@end

@implementation ZJVideoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.videoArr = [NSMutableArray arrayWithObjects:@"http://static.tripbe.com/videofiles/20121214/9533522808.f4v.mp4",@"http://archive.org/download/WaltDisneyCartoons-MickeyMouseMinnieMouseDonaldDuckGoofyAndPluto/WaltDisneyCartoons-MickeyMouseMinnieMouseDonaldDuckGoofyAndPluto-HawaiianHoliday1937-Video.mp4",@"http://219.239.26.20/download/53546556/76795884/2/dmg/232/4/1383696088040_516/QQ_V3.0.1.dmg", @"http://219.239.26.11/download/46280417/68353447/3/dmg/105/192/1369883541097_192/KindleForMac._V383233429_.dmg",nil];
    
    if (IOS_VERSION >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    [self uploadGripView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)uploadGripView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 0, 300, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = RGBACOLOR(60, 60, 60, 1);
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 12)];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = tableHeaderView;
    
    UIView *tableFooterView = [[UIView alloc] init];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = tableFooterView;
    
    [self.view addSubview:self.tableView];

}

#pragma mark - Action

- (void)clickDownload:(UIButton *)button
{
    NSString *urlStr = [self.videoArr objectAtIndex:button.tag];
    NSString *fileName = [urlStr lastPathComponent];
    NSString *filePath = [fileName pathExtension];
    NSLog(@"fileName:%@ filePath:%@",fileName,filePath);
    [[FilesDownManage sharedFilesDownManage] downFileUrl:urlStr filename:fileName filetarget:filePath fileimage:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.videoArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifity = @"Cell";
    
    ZJCameraListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifity];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ZJCameraListCell" owner:nil options:nil];
        cell = [nib objectAtIndex:0];
        
        cell.backgroundColor = [UIColor clearColor];
        [cell.downLoadButton addTarget:self action:@selector(clickDownload:) forControlEvents:UIControlEventTouchDown];
    }
    
    NSString *videoUrl = [self.videoArr objectAtIndex:indexPath.row];
    cell.mediaNameLabel.text = [videoUrl lastPathComponent];
    cell.downLoadButton.tag = indexPath.row;
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self playVideo:indexPath.row];
}

#pragma mark - Action

- (void)playVideo:(NSInteger)videoIndex
{
    NSURL *url = [NSURL URLWithString:[self.videoArr objectAtIndex:videoIndex]];    
    ZJMoviePlayerViewController *playerViewController =[[ZJMoviePlayerViewController alloc]initWithContentURL:url];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myMovieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:playerViewController.moviePlayer];
    [playerViewController.moviePlayer play];
    [self presentModalViewController:playerViewController animated:NO];
    
//    ZJVideoPlayerVC *videoPlayerVC = [[ZJVideoPlayerVC alloc] init];
//    videoPlayerVC.videoArr = self.videoArr;
//    videoPlayerVC.currentVideoIndex = videoIndex;
//    [self presentViewController:videoPlayerVC animated:NO completion:nil];
}

-(void)myMovieFinishedCallback:(NSNotification*)notification
{
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ZJLocalizedString(@"Hint") message:ZJLocalizedString(@"Play ends") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

@end
