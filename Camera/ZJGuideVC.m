//
//  ZJGuideVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJGuideVC.h"

@interface ZJGuideVC ()

@end

@implementation ZJGuideVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = BACKGROUNDCOLOR;
    
    [self uploadView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

#pragma mark - UI

- (void)uploadView
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.contentSize = CGSizeMake(320*3, 0);
    [self.view addSubview:scrollView];
    
    for (int i = 0; i < 3; i++) {
        
        NSString *imageName = [NSString stringWithFormat:@"guide%d",i+1];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(320*i, 0, 320, scrollView.frame.size.height)];
        imageView.contentMode = UIViewContentModeCenter;
        imageView.image = [UIImage imageNamed:imageName];
        imageView.userInteractionEnabled = YES;
        [scrollView addSubview:imageView];
        
        if (i == 2) {
            
            UIButton *startButton = [UIButton buttonWithType:UIButtonTypeCustom];
            startButton.frame = CGRectMake((320-60)/2, self.view.frame.size.height-95, 60, 30);
            startButton.showsTouchWhenHighlighted = YES;
            [startButton setTitle:ZJLocalizedString(@"START")  forState:UIControlStateNormal];
            startButton.titleLabel.font = [UIFont systemFontOfSize:14];
            [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            startButton.layer.borderColor = [UIColor whiteColor].CGColor;
            startButton.layer.borderWidth = 1.0;
            startButton.layer.cornerRadius = 2;
            [startButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
            [imageView addSubview:startButton];
        }
    }
    
    self.pageControl = [[SMPageControl alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-35, self.view.frame.size.width, 20)];
    self.pageControl.userInteractionEnabled = NO;
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = 3;
    self.pageControl.indicatorMargin = 5.0f;
    self.pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"icon_redpoint"];
    self.pageControl.pageIndicatorImage = [UIImage imageNamed:@"icon_whitepoint"];
    self.pageControl.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.pageControl];
}

#pragma mark - Action

- (void)dismiss
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page >= 3 || page<0) return;
    
    self.pageControl.currentPage = page;
}
@end
