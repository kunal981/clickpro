//
//  ZJSetttingVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJSetttingVC.h"
#import "ZJConfigListVC.h"
#import "ZJAppDelegate.h"
#import "MJRefresh.h"

@interface ZJSetttingVC ()<ZJConfigListViewDelegate,MJRefreshBaseViewDelegate>
{
    MJRefreshHeaderView *_header;
}
@end

@implementation ZJSetttingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = RGBACOLOR(236, 236, 236, 1);
   
    NSString *path=[[NSBundle mainBundle] pathForResource:@"Config"
                                                   ofType:@"plist"];
    self.titleArr = [[NSArray alloc]
                    initWithContentsOfFile:path];
    
    self.config = [ZJConfigClass sharedInstance];

    [self uploadNav];
    [self uploadView];
}

- (void)viewWillAppear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)uploadNav
{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 29, 29);
    [backButton setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(clickBackButton) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.title = @"Setting";
}

- (void)uploadView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 12, 300, self.view.frame.size.height-12) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
//    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 12)];
//    tableHeaderView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = tableHeaderView;
    
    UIView *tableFooterView = [[UIView alloc] init];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = tableFooterView;
    
    [self.view addSubview:self.tableView];
    
    // 3.3行集成下拉刷新控件
    _header = [MJRefreshHeaderView header];
    _header.scrollView = self.tableView;
    _header.delegate = self;
    
    // 5.0.5秒后自动下拉刷新
    [_header performSelector:@selector(beginRefreshing) withObject:nil afterDelay:0.5];
}

#pragma mark - Action

- (void)clickBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - MJRefreshBaseViewDelegate

- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView
{
    [self.config getCofigSetting:self];
}

#pragma mark - Data

- (void)formartSD
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(formartSDFinished:);
    interface.didFinishedError = @selector(formartSDFinishedError);
    [interface formatSD];
}

- (void)formartSDFinished:(id)data
{
    [[SDImageCache sharedImageCache] clearMemory];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showSuccess:ZJLocalizedString(@"Completed") toView:nil];
}

- (void)formartSDFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showError:ZJLocalizedString(@"Failed") toView:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
      
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];

        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textColor = RGBACOLOR(28, 28, 28, 1);
        cell.textLabel.highlightedTextColor = RGBACOLOR(28, 28, 28, 1);
        
        cell.detailTextLabel.font = [UIFont systemFontOfSize:13];
        cell.detailTextLabel.textColor = RGBACOLOR(165, 165, 165, 1);
        cell.detailTextLabel.highlightedTextColor = RGBACOLOR(165, 165, 165, 1);
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSDictionary *dic = [self.titleArr objectAtIndex:indexPath.row];
    cell.textLabel.text = ZJLocalizedString([dic objectForKey:@"title"]);
    
    if (indexPath.row == 0) {
        cell.detailTextLabel.text = self.config.videoResolution;
    }else if (indexPath.row == 1){
        
        cell.detailTextLabel.text = self.config.photoSize;

        if ([self.config.burstMode isEqualToString:@"off"]) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }else if (indexPath.row == 2){
        cell.detailTextLabel.text = self.config.dualStreams;
    }else if (indexPath.row == 3){
        cell.detailTextLabel.text = self.config.videoStamp;
    }else if (indexPath.row == 4){
        cell.detailTextLabel.text = self.config.burstMode;
    }else if (indexPath.row == 5){
        cell.detailTextLabel.text = self.config.photoStamp;
    }else if (indexPath.row == 6){
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 2) {
        
        if (![self.config.videoStamp isEqualToString:@"off"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ZJLocalizedString(@"Hint") message:ZJLocalizedString(@"Please set video stamp off first!") delegate:nil cancelButtonTitle:ZJLocalizedString(@"YES") otherButtonTitles:nil, nil];
            [alert show];
            
            return;
        }
    }else if (indexPath.row == 3) {
        
        if ([self.config.dualStreams isEqualToString:@"on"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ZJLocalizedString(@"Hint") message:ZJLocalizedString(@"Please set dual stream off first!") delegate:nil cancelButtonTitle:ZJLocalizedString(@"YES") otherButtonTitles:nil, nil];
            [alert show];
            
            return;
        }
    }
    
    NSDictionary *dic = [self.titleArr objectAtIndex:indexPath.row];

    BOOL isSubType = [[dic objectForKey:@"subType"] boolValue];
    
    if (isSubType) {
        
        if (indexPath.row == 1 && ![self.config.burstMode isEqualToString:@"off"]) {
            return;
        }
        
        ZJConfigListVC *configListVC = [[ZJConfigListVC alloc] init];
        configListVC.delegate = self;
        configListVC.settingIndex = indexPath.row;
        configListVC.configDic = dic;
        if (indexPath.row == 0) {
            configListVC.currentIndex = [self.config.videoResolutionArr indexOfObject:self.config.videoResolution];
            configListVC.titleArr = self.config.videoResolutionArr;
        }else if (indexPath.row == 1){
            configListVC.currentIndex = [self.config.photoSizeArr indexOfObject:self.config.photoSize];
            configListVC.titleArr = self.config.photoSizeArr;
        }else if (indexPath.row == 2){
            configListVC.currentIndex = [self.config.dualStreamsArr indexOfObject:self.config.dualStreams];
            configListVC.titleArr = self.config.dualStreamsArr;
        }else if (indexPath.row == 3){
            configListVC.currentIndex = [self.config.videoStampArr indexOfObject:self.config.videoStamp];
            configListVC.titleArr = self.config.videoStampArr;
        }else if (indexPath.row == 4){
            configListVC.currentIndex = [self.config.burstModeArr indexOfObject:self.config.burstMode];
            configListVC.titleArr = self.config.burstModeArr;
        }else if (indexPath.row == 5){
            configListVC.currentIndex = [self.config.photoStampArr indexOfObject:self.config.photoStamp];
            configListVC.titleArr = self.config.photoStampArr;
        }
        [self.navigationController pushViewController:configListVC animated:YES];
    }else{
    
        if (indexPath.row == 6) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:ZJLocalizedString(@"Format SD notice") delegate:self cancelButtonTitle:nil otherButtonTitles:ZJLocalizedString(@"YES"),ZJLocalizedString(@"NO"), nil];
            [alert show];
        }else if (indexPath.row == 7){
        
            ZJAppDelegate *appDelegate = (ZJAppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate showGuide];
        }
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self formartSD];
    }
}

#pragma mark - ZJConfigListViewDelegate

- (void)zjConfigListVC:(ZJConfigListVC *)zjConfigList didSuccessConfigWithConfigTye:(NSString *)configTye SettingIndex:(NSInteger)settingIndex
{
    if (settingIndex == 4) {
        // 5.0.5秒后自动下拉刷新
        [_header performSelector:@selector(beginRefreshing) withObject:nil afterDelay:0.0];
    }else{
    
        if (settingIndex == 0) {
            self.config.videoResolution = configTye;
        }else if (settingIndex == 1){
            self.config.photoSize = configTye;
        }else if (settingIndex == 2){
            self.config.dualStreams = configTye;
        }else if (settingIndex == 3){
            self.config.videoStamp = configTye;
        }else if (settingIndex == 5){
            self.config.photoStamp = configTye;
        }
        
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:settingIndex inSection:0], nil] withRowAnimation:UITableViewRowAnimationFade];
    }
}


#pragma mark - ZJConfigDelegate

- (void)getConfigFinished
{
    [self.tableView reloadData];
    [_header endRefreshing];
}

- (void)getConfigFinishedError
{
    [MBProgressHUD showError:ZJLocalizedString(@"Failed") toView:nil];
    [_header endRefreshing];
}

@end
