//
//  ZJSetttingVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ZJInterfaceEntry.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+Add.h"

@class ZJConfigListVC;

@protocol ZJConfigListViewDelegate <NSObject>

- (void)zjConfigListVC:(ZJConfigListVC *)zjConfigList
didSuccessConfigWithConfigTye:(NSString *)configTye
          SettingIndex:(NSInteger)settingIndex;

@end

@interface ZJConfigListVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    int configIndex;
}

@property (nonatomic, weak) id <ZJConfigListViewDelegate>delegate;
@property (nonatomic) NSInteger currentIndex;
@property (nonatomic) NSInteger settingIndex;
@property (nonatomic, strong) NSDictionary *configDic;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) UITableView *tableView;

@end
