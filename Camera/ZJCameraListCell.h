//
//  ZJCameraListCell.h
//  Camera
//
//  Created by Apple_ZJ on 13-12-21.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJCameraListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *mediaNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *downLoadButton;
@end
