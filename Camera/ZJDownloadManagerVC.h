//
//  ZJDownloadManagerVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-10-24.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "DownloadCell.h"
#import "FinishedCell.h"
#import "DownloadDelegate.h"

@interface ZJDownloadManagerVC : UIViewController<UITableViewDataSource, UITableViewDelegate,DownloadDelegate,UIAlertViewDelegate>
{
    FileModel *currentFileModel;
}
@property(nonatomic,retain) UITableView *downloadingTable;
@property(nonatomic,retain) UITableView *finishedTable;
@property(nonatomic,retain) NSMutableArray *downingList;
@property(nonatomic,retain) NSMutableArray *finishedList;

@end
