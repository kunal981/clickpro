//
//  ZJVideoPlayerVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-11-11.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJVideoPlayerVC.h"
#import "ZJVideoPlayView.h"

#define PLAYER_CONTROL_BAR_HEIGHT 100
#define TOPVIEW_HEIGHT 44

@interface ZJVideoPlayerVC ()

@property (nonatomic, strong) ZJVideoPlayView *videoPlayView;
@property (nonatomic, strong) AVPlayer *videoPlayer;

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) UIButton *downLoadButton;

@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *previousButton;
@property (nonatomic, strong) UIButton *playPauseButton;
@property (nonatomic, strong) UIButton *nextButton;

@property (nonatomic, strong) UISlider *videoScrubber;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIProgressView *progressView;

@property (readwrite, strong) id scrubberTimeObserver;
@property (readwrite, strong) id playClockTimeObserver;
@end

@implementation ZJVideoPlayerVC

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self removeObserversFromVideoPlayerItem];
    [self removePlayerTimeObservers];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    [self addTopView];
    [self addBottomView];
    
    UITapGestureRecognizer *playerTouchedGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoTapHandler)];
    playerTouchedGesture.delegate = self;
    [self.view addGestureRecognizer:playerTouchedGesture];
    
    
    self.videoPlayView = [[ZJVideoPlayView alloc] initWithFrame:self.view.bounds];
    self.videoPlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.videoPlayView];
    [self.view sendSubviewToBack:self.videoPlayView];
    
    [self updateVideo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

#pragma mark - UI

- (void)addTopView
{
    CGRect bounds = [self.view bounds];

    //顶部View
    _topView = [[UIView alloc] init];
    _topView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_topView setFrame:CGRectMake(0, 0, bounds.size.width, TOPVIEW_HEIGHT+STATUS_HEIGHT)];
    [_topView setOpaque:NO];
    [_topView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    [self.view addSubview:_topView];
    
    _closeButton = [[UIButton alloc] init];
    [_closeButton setFrame:CGRectMake(10, 7+STATUS_HEIGHT, 30, 30)];
    [_closeButton setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [_closeButton addTarget:self action:@selector(clickCloseButton) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_closeButton];
    
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [_titleLabel setFrame:CGRectMake(40,STATUS_HEIGHT,bounds.size.width-40,44)];
    _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_titleLabel setFont:[UIFont fontWithName:@"Forza-Medium" size:13.0f]];
    [_titleLabel setTextColor:RGBACOLOR(254, 254, 254, 1)];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    [_topView addSubview:_titleLabel];
    
//    _deleteButton = [[UIButton alloc] init];
//    _deleteButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
//    [_deleteButton setFrame:CGRectMake(bounds.size.width-75, 7, 30, 30)];
//    [_deleteButton setImage:[UIImage imageNamed:@"btn_delete"] forState:UIControlStateNormal];
//    [_topView addSubview:_deleteButton];
    
    _downLoadButton = [[UIButton alloc] init];
    _downLoadButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [_downLoadButton setFrame:CGRectMake(bounds.size.width-30-10, 7+STATUS_HEIGHT, 30, 30)];
    [_downLoadButton setImage:[UIImage imageNamed:@"btn_download"] forState:UIControlStateNormal];
    [_downLoadButton setImage:[UIImage imageNamed:@"icon_check"] forState:UIControlStateDisabled];
    _downLoadButton.contentMode = UIViewContentModeScaleAspectFit;
    [_topView addSubview:_downLoadButton];
    _downLoadButton.hidden = YES;
}

- (void)addBottomView
{
    CGRect bounds = [self.view bounds];

    //控制View
    _bottomView = [[UIView alloc] init];
    _bottomView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    [_bottomView setFrame:CGRectMake(bounds.origin.x,
                                           bounds.size.height - PLAYER_CONTROL_BAR_HEIGHT,
                                           bounds.size.width,
                                           PLAYER_CONTROL_BAR_HEIGHT)];
    [_bottomView setOpaque:NO];
    [_bottomView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    [self.view addSubview:_bottomView];
    
    _previousButton = [[UIButton alloc] init];
    [_previousButton setFrame:CGRectMake(10,11,30,30)];
    [_previousButton setImage:[UIImage imageNamed:@"btn_previous"] forState:UIControlStateNormal];
    [_previousButton addTarget:self action:@selector(clickPreviousButton) forControlEvents:UIControlEventTouchUpInside];
    [_previousButton setShowsTouchWhenHighlighted:YES];
    [_bottomView addSubview:_previousButton];
    if (self.currentVideoIndex == 0){
        _previousButton.enabled = NO;
    }
    
    _playPauseButton = [[UIButton alloc] init];
    [_playPauseButton setFrame:CGRectMake(53,5,42,42)];
    [_playPauseButton setImage:[UIImage imageNamed:@"btn_play"] forState:UIControlStateNormal];
    [_playPauseButton setShowsTouchWhenHighlighted:YES];
    [_playPauseButton addTarget:self action:@selector(clickPlayPauseButton) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:_playPauseButton];
    
    _nextButton = [[UIButton alloc] init];
    [_nextButton setFrame:CGRectMake(110,11,30,30)];
    [_nextButton setImage:[UIImage imageNamed:@"btn_next"] forState:UIControlStateNormal];
    [_nextButton setShowsTouchWhenHighlighted:YES];
    [_nextButton addTarget:self action:@selector(clickNextButton) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:_nextButton];
    if (self.currentVideoIndex >= self.videoArr.count-1) {
        _nextButton.enabled = NO;
    }
    
    _videoScrubber = [[UISlider alloc] init];
    [_videoScrubber setFrame:CGRectMake(15, 46, bounds.size.width-30, 34)];
    _videoScrubber.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _videoScrubber.backgroundColor = [UIColor clearColor];
    [_videoScrubber setMinimumTrackTintColor:[UIColor whiteColor]];
    [_videoScrubber setMaximumTrackTintColor:[UIColor clearColor]];
    [_videoScrubber setThumbImage:[UIImage imageNamed:@"player-progress-point"] forState:UIControlStateNormal];
    [_bottomView addSubview:_videoScrubber];
    //播放进度
    [_videoScrubber addTarget:self action:@selector(scrubbingDidBegin) forControlEvents:UIControlEventTouchDown];
    
    [_videoScrubber addTarget:self action:@selector(scrubberIsScrolling) forControlEvents:UIControlEventValueChanged];
    [_videoScrubber addTarget:self action:@selector(scrubbingDidEnd) forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchCancel)];
    
    _progressView = [[UIProgressView alloc] init];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_progressView setBounds:CGRectMake(0, 0, _videoScrubber.bounds.size.width-3, _videoScrubber.bounds.size.height)];
    [_progressView setCenter:CGPointMake(_videoScrubber.bounds.size.width/2+3, _videoScrubber.bounds.size.height/2)];
    _progressView.progressTintColor = [UIColor colorWithRed:31.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0];
    _progressView.userInteractionEnabled = NO;
    _progressView.progressTintColor = [UIColor redColor];
    _progressView.trackTintColor = [UIColor lightGrayColor];
    [_videoScrubber addSubview:_progressView];
    
    _timeLabel = [[UILabel alloc] init];
    [_timeLabel setFrame:CGRectMake(15,80,150,14)];
    [_timeLabel setBackgroundColor:[UIColor clearColor]];
    [_timeLabel setTextColor:RGBACOLOR(254, 254, 254, 1)];
    [_timeLabel setFont:[UIFont systemFontOfSize:13]];
    [_timeLabel setTextAlignment:NSTextAlignmentLeft];
    [_bottomView addSubview:_timeLabel];
}

- (void)updateVideo
{    
    [self.videoPlayView.activityIndicator startAnimating];
    // Reset the buffer bar back to 0
    [_progressView setProgress:0 animated:NO];
    [self showControls];
    
    NSString *video = [self.videoArr objectAtIndex:self.currentVideoIndex];
    NSURL *videoURL = [NSURL URLWithString:video];
    
    NSLog(@"播放videoURL:%@",videoURL);
    
    NSString *title = [video lastPathComponent];
    _titleLabel.text = title;
    
    [_timeLabel setText:[NSString stringWithFormat:@"--:--/--:--"]];
    
    _videoScrubber.value = 0;
    
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:@{
                                                                MPMediaItemPropertyTitle: title,
                                                                }];
    
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:videoURL];
    
    [playerItem addObserver:self
                 forKeyPath:@"status"
                    options:NSKeyValueObservingOptionNew
                    context:nil];
    
    [playerItem addObserver:self
                 forKeyPath:@"playbackBufferEmpty"
                    options:NSKeyValueObservingOptionNew
                    context:nil];
    
    [playerItem addObserver:self
                 forKeyPath:@"playbackLikelyToKeepUp"
                    options:NSKeyValueObservingOptionNew
                    context:nil];
    
    [playerItem addObserver:self
                 forKeyPath:@"loadedTimeRanges"
                    options:NSKeyValueObservingOptionNew
                    context:nil];
    
    if (!self.videoPlayer) {
        _videoPlayer = [AVPlayer playerWithPlayerItem:playerItem];
        [_videoPlayer setAllowsAirPlayVideo:YES];
        [_videoPlayer setUsesAirPlayVideoWhileAirPlayScreenIsActive:YES];
        
        if ([_videoPlayer respondsToSelector:@selector(setAllowsExternalPlayback:)]) { // iOS 6 API
            [_videoPlayer setAllowsExternalPlayback:YES];
        }
        
        [_videoPlayView setPlayer:_videoPlayer];
    } else {
        [self removeObserversFromVideoPlayerItem];
        [self.videoPlayer replaceCurrentItemWithPlayerItem:playerItem];
    }
    
    // iOS 5
    [_videoPlayer addObserver:self forKeyPath:@"airPlayVideoActive" options:NSKeyValueObservingOptionNew context:nil];
    
    // iOS 6
    [_videoPlayer addObserver:self
                   forKeyPath:@"externalPlaybackActive"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.videoPlayer.currentItem];
    
    [self syncPlayPauseButtons];
}

#pragma mark - Action

- (void)clickCloseButton
{
    [self.videoPlayer pause];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)clickPlayPauseButton
{
    if (_seekToZeroBeforePlay) {
        _seekToZeroBeforePlay = NO;
        [_videoPlayer seekToTime:kCMTimeZero];
    }
    
    if ([self isPlaying]) {
        [_videoPlayer pause];
    } else {
        [self playVideo];
        [self.videoPlayView.activityIndicator stopAnimating];
    }
    
    [self syncPlayPauseButtons];
    [self showControls];
}

- (void)clickPreviousButton
{
    self.currentVideoIndex--;
    
    if (self.currentVideoIndex <= 0) {
        self.currentVideoIndex = 0;
        _previousButton.enabled = NO;
    }
    
    _nextButton.enabled = YES;
    
    [self syncPlayPauseButtons];
    [self updateVideo];
}


- (void)clickNextButton
{
    self.currentVideoIndex++;
    
    if (self.currentVideoIndex >= self.videoArr.count-1) {
        self.currentVideoIndex = self.videoArr.count-1;
        _nextButton.enabled = NO;
    }
    
    _previousButton.enabled = YES;
    [self syncPlayPauseButtons];
    [self updateVideo];
}

-(void)scrubbingDidBegin
{
    if ([self isPlaying]) {
        [_videoPlayer pause];
        [self syncPlayPauseButtons];
        self.restoreVideoPlayStateAfterScrubbing = YES;
        [self showControls];
    }
}

-(void)scrubberIsScrolling
{
    CMTime playerDuration = [self playerItemDuration];
    double duration = CMTimeGetSeconds(playerDuration);
    
    if (isfinite(duration)) {
        double currentTime = floor(duration * _videoScrubber.value);
        //double timeLeft = floor(duration - currentTime);
        
        if (currentTime <= 0) {
            currentTime = 0;
        }
        
        [_timeLabel setText:[NSString stringWithFormat:@"%@/%@",[self stringFormattedTimeFromSeconds:&currentTime],[self stringFormattedTimeFromSeconds:&duration]]];
        
        [_videoPlayer seekToTime:CMTimeMakeWithSeconds((float) currentTime, NSEC_PER_SEC)];
    }
}

-(void)scrubbingDidEnd
{
    if (self.restoreVideoPlayStateAfterScrubbing) {
        self.restoreVideoPlayStateAfterScrubbing = NO;
        scrubBuffering = YES;
    }
    [self.videoPlayView.activityIndicator startAnimating];
    
    [self showControls];
}

- (void)videoTapHandler
{
    if (_bottomView.alpha) {
        [self hideControlsAnimated:YES];
    } else {
        [self showControls];
    }
}

- (void)playVideo
{
    self.playerIsBuffering = NO;
    scrubBuffering = NO;
    playWhenReady = NO;
    // Configuration is done, ready to start.
    [self.videoPlayer play];
    [self updatePlaybackProgress];
}

- (void)syncPlayPauseButtons
{
    if ([self isPlaying]) {
        [_playPauseButton setImage:[UIImage imageNamed:@"btn_pause"] forState:UIControlStateNormal];
    } else {
        [_playPauseButton setImage:[UIImage imageNamed:@"btn_play"] forState:UIControlStateNormal];
    }
}

- (void)showControls
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"VideoPlayerWillShowControlsNotification"
                                                        object:self
                                                      userInfo:nil];
    [UIView animateWithDuration:0.4 animations:^{
        _topView.alpha = 1.0;
        _bottomView.alpha = 1.0;
    } completion:nil];
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideControlsAnimated:) object:@YES];
    
    if ([self isPlaying]) {
        [self performSelector:@selector(hideControlsAnimated:) withObject:@YES afterDelay:4.0];
    }
}

- (void)hideControlsAnimated:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"VideoPlayerWillHideControlsNotitication"
                                                        object:self
                                                      userInfo:nil];
    if (animated) {
        [UIView animateWithDuration:0.4 animations:^{
            _topView.alpha = 0;
            _bottomView.alpha = 0;
        } completion:nil];
    } else {
        _topView.alpha = 0;
        _bottomView.alpha = 0;
    }
}

- (void)updatePlaybackProgress
{
    [self syncPlayPauseButtons];
    [self showControls];
    
    double interval = .1f;
    
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration)) {
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (CMTIME_IS_INDEFINITE(playerDuration) || duration <= 0) {
        [_videoScrubber setHidden:YES];
        [_progressView setHidden:YES];
        [self syncPlayClock];
        return;
    }
    
    [_videoScrubber setHidden:NO];
    [_progressView setHidden:NO];
    
    CGFloat width = CGRectGetWidth([_videoScrubber bounds]);
    interval = 0.5f * duration / width;
    __weak ZJVideoPlayerVC *vpvc = self;
    _scrubberTimeObserver = [_videoPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                                       queue:NULL
                                                                  usingBlock:^(CMTime time) {
                                                                      [vpvc syncScrubber];
                                                                  }];
    
    // Update the play clock every second
    _playClockTimeObserver = [_videoPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1, NSEC_PER_SEC)
                                                                        queue:NULL
                                                                   usingBlock:^(CMTime time) {
                                                                       [vpvc syncPlayClock];
                                                                   }];
    
}

- (void)syncScrubber
{
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration)) {
        _videoScrubber.value = 0.0;
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration)) {
        float minValue = [_videoScrubber minimumValue];
        float maxValue = [_videoScrubber maximumValue];
        double time = CMTimeGetSeconds([_videoPlayer currentTime]);
        
        [_videoScrubber setValue:(maxValue - minValue) * time / duration + minValue];
    }
}

- (void)syncPlayClock
{
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration)) {
        return;
    }
    
    if (CMTIME_IS_INDEFINITE(playerDuration)) {
        [_timeLabel setText:[NSString stringWithFormat:@"--:--/--:--"]];
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    
    if (isfinite(duration)) {
        double currentTime = floor(CMTimeGetSeconds([_videoPlayer currentTime]));
        //        double timeLeft = floor(duration - currentTime);
        if (currentTime <= 0) {
            currentTime = 0;
        }
        [_timeLabel setText:[NSString stringWithFormat:@"%@/%@",[self stringFormattedTimeFromSeconds:&currentTime],[self stringFormattedTimeFromSeconds:&duration]]];
    }
}

#pragma mark - 通知

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    NSLog(@"==播放结束==");
    [self syncPlayPauseButtons];
    
    _seekToZeroBeforePlay = YES;

//    [_playPauseButton setImage:[UIImage imageNamed:@"btn_play"] forState:UIControlStateNormal];
}

#pragma mark - UIGestureRecognizer

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_topView] || [touch.view isDescendantOfView:_bottomView]) {
        return NO;
    }
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - AVPlayer代理

// Wait for the video player status to change to ready before initializing video player controls
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == _videoPlayer
        && ([keyPath isEqualToString:@"externalPlaybackActive"] || [keyPath isEqualToString:@"airPlayVideoActive"])) {
        return;
    }
    
    if (object != [_videoPlayer currentItem]) {
        return;
    }
    
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        switch (status) {
            case AVPlayerStatusReadyToPlay:
                playWhenReady = YES;
                break;
            case AVPlayerStatusFailed:
                // TODO:
                [self removeObserversFromVideoPlayerItem];
                [self removePlayerTimeObservers];
                self.videoPlayer = nil;
                NSLog(@"failed");
                break;
        }
    } else if ([keyPath isEqualToString:@"playbackBufferEmpty"] && _videoPlayer.currentItem.playbackBufferEmpty) {
        self.playerIsBuffering = YES;
        [self.videoPlayView.activityIndicator startAnimating];
        [self syncPlayPauseButtons];
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"] && _videoPlayer.currentItem.playbackLikelyToKeepUp) {
        if (![self isPlaying] && (playWhenReady || self.playerIsBuffering || scrubBuffering)) {
            [self playVideo];
        }
        [self.videoPlayView.activityIndicator stopAnimating];
    } else if ([keyPath isEqualToString:@"loadedTimeRanges"]) {
        float durationTime = CMTimeGetSeconds([[self.videoPlayer currentItem] duration]);
        float bufferTime = [self availableDuration];
        [_progressView setProgress:bufferTime/durationTime animated:YES];
    }

    return;
}

#pragma mark - 清空通知

- (void)removeObserversFromVideoPlayerItem
{
    [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"status"];
    [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
    [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
    
    [_videoPlayer removeObserver:self forKeyPath:@"externalPlaybackActive"];
    [_videoPlayer removeObserver:self forKeyPath:@"airPlayVideoActive"];
}

-(void)removePlayerTimeObservers
{
    if (_scrubberTimeObserver) {
        [_videoPlayer removeTimeObserver:_scrubberTimeObserver];
        _scrubberTimeObserver = nil;
    }
    
    if (_playClockTimeObserver) {
        [_videoPlayer removeTimeObserver:_playClockTimeObserver];
        _playClockTimeObserver = nil;
    }
}

#pragma mark -

- (NSString *)stringFormattedTimeFromSeconds:(double *)seconds
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:*seconds];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    if (*seconds >= 3600) {
        [formatter setDateFormat:@"HH:mm:ss"];
    } else {
        [formatter setDateFormat:@"mm:ss"];
    }
    
    return [formatter stringFromDate:date];
}


- (float)availableDuration
{
    NSArray *loadedTimeRanges = [[self.videoPlayer currentItem] loadedTimeRanges];
    
    // Check to see if the timerange is not an empty array, fix for when video goes on airplay
    // and video doesn't include any time ranges
    if ([loadedTimeRanges count] > 0) {
        CMTimeRange timeRange = [[loadedTimeRanges objectAtIndex:0] CMTimeRangeValue];
        float startSeconds = CMTimeGetSeconds(timeRange.start);
        float durationSeconds = CMTimeGetSeconds(timeRange.duration);
        return (startSeconds + durationSeconds);
    } else {
        return 0.0f;
    }
}

- (CMTime)playerItemDuration
{
    if (_videoPlayer.status == AVPlayerItemStatusReadyToPlay) {
        return([_videoPlayer.currentItem duration]);
    }
    
    return(kCMTimeInvalid);
}

- (BOOL)isPlaying
{
    return [_videoPlayer rate] != 0.0;
}

@end
