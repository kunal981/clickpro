//
//  UINavigationController+Rotation.h
//  GuMei
//
//  Created by Apple_ZJ on 13-6-18.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Rotation)

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation;
-(BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
@end
