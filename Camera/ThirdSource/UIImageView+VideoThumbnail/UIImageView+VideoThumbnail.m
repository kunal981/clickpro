//
//  UIImageView+VideoThumbnail.m
//  CompanyInformation
//
//  Created by Apple_ZJ on 13-7-4.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "UIImageView+VideoThumbnail.h"
#import "SDImageCache.h"

@implementation UIImageView (VideoThumbnail)

-(void)generateImageWithUrl:(NSString *)urlStr placeholderImage:(UIImage *)placeholder
{
    [self setImage:placeholder];
    
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:urlStr];
    
    if (image != nil) {
        [self setImage:image];
        return;
    }

    AVURLAsset *asset=[[AVURLAsset alloc] initWithURL:[NSURL URLWithString:urlStr] options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform=TRUE;

    CMTime thumbTime = CMTimeMakeWithSeconds(3,60);
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime,  AVAssetImageGeneratorResult result, NSError *error)
    {
        if (result == AVAssetImageGeneratorFailed)
        {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }
        if (result==AVAssetImageGeneratorSucceeded)
        {
            UIImage *theThumbnailsImage=[UIImage imageWithCGImage:im];
            [self setImage:theThumbnailsImage];
            
            [[SDImageCache sharedImageCache] storeImage:theThumbnailsImage forKey:urlStr];
        }
    };
    
    CGSize maxSize = CGSizeMake(self.bounds.size.width*2, self.bounds.size.height*2);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
}

@end
