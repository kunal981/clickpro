//
//  UIImageView+VideoThumbnail.h
//  CompanyInformation
//
//  Created by Apple_ZJ on 13-7-4.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface UIImageView (VideoThumbnail)

-(void)generateImageWithUrl:(NSString *)urlStr placeholderImage:(UIImage *)placeholder;
@end
