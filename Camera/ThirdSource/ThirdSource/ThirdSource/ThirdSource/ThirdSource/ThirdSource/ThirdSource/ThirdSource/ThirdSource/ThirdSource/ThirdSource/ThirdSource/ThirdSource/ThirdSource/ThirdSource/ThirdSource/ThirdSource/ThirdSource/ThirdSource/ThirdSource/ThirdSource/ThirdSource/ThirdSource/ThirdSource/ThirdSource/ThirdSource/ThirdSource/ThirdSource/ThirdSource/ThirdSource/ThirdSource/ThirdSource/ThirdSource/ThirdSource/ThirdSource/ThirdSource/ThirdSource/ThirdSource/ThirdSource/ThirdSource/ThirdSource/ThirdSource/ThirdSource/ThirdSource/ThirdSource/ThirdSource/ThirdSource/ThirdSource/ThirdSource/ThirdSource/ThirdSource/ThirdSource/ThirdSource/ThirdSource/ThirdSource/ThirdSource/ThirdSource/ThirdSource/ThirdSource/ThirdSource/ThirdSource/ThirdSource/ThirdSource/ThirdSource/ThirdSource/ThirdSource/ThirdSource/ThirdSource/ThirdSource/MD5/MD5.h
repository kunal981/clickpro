//
//  MD5.h
//  CompanyInformation
//
//  Created by Apple_ZJ on 13-7-5.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MD5 : NSObject

+ (NSString *)md5Digest:(NSString *)key;
@end
