//
//  MJPhotoBrowser.h
//
//  Created by mj on 13-3-4.
//  Copyright (c) 2013年 itcast. All rights reserved.

#import <UIKit/UIKit.h>

@interface MJPhotoBrowser : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, assign) NSUInteger currentPhotoIndex;
@end
