//
//  MJPhotoBrowser.m
//
//  Created by mj on 13-3-4.
//  Copyright (c) 2013年 itcast. All rights reserved.

#import <QuartzCore/QuartzCore.h>
#import "MJPhotoBrowser.h"
#import "MJPhoto.h"
#import "SDWebImageManager.h"
#import "MJPhotoView.h"
#import "MBProgressHUD+Add.h"
#import "MBProgressHUD.h"

#define kPadding 10
#define kPhotoViewTagOffset 1000
#define kPhotoViewIndex(photoView) ([photoView tag] - kPhotoViewTagOffset)

#define PLAYER_CONTROL_BAR_HEIGHT 44
#define TOPVIEW_HEIGHT 44

@interface MJPhotoBrowser () <MJPhotoViewDelegate>
{
    // 滚动的view
	UIScrollView *_photoScrollView;
    // 所有的图片view
	 NSMutableSet *_visiblePhotoViews;
    NSMutableSet *_reusablePhotoViews;
    
    BOOL isControlViewHidden;
    
    UIView *_topView;
    UIButton *_closeButton;
    UILabel *_titleLabel;
    UIButton *_deleteButton;
    UIButton *_downLoadButton;
    
    UIView *_bottomView;
    UIButton *_previousButton;
    UIButton *_nextButton;
}
@end

@implementation MJPhotoBrowser

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 1.创建UIScrollView
    [self createScrollView];
    
    [self createTopView];
    [self createBottomView];
    
    [self showPhotos];
}

#pragma mark - 创建工具条

- (void)createTopView
{
    _topView = [[UIView alloc] init];
    [_topView setFrame:CGRectMake(0, 20, self.view.frame.size.width, TOPVIEW_HEIGHT)];
    [_topView setOpaque:NO];
    [_topView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    [self.view addSubview:_topView];
    
    _closeButton = [[UIButton alloc] init];
    [_closeButton setFrame:CGRectMake(10, 7, 30, 30)];
    [_closeButton setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [_closeButton addTarget:self action:@selector(clickCloseButton) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_closeButton];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [_titleLabel setFrame:CGRectMake(45,7,self.view.bounds.size.width-90,30)];
    [_titleLabel setFont:[UIFont fontWithName:@"Forza-Medium" size:13.0f]];
    [_titleLabel setTextColor:RGBACOLOR(254, 254, 254, 1)];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [_topView addSubview:_titleLabel];
    
    _deleteButton = [[UIButton alloc] init];
    [_deleteButton setFrame:CGRectMake(self.view.bounds.size.width-75, 7, 30, 30)];
    [_deleteButton setImage:[UIImage imageNamed:@"btn_delete"] forState:UIControlStateNormal];
    [_topView addSubview:_deleteButton];
    
    _downLoadButton = [[UIButton alloc] init];
    [_downLoadButton setFrame:CGRectMake(self.view.bounds.size.width-30-10, 7, 30, 30)];
    [_downLoadButton setImage:[UIImage imageNamed:@"btn_download"] forState:UIControlStateNormal];
    [_downLoadButton addTarget:self action:@selector(clickDownLoadButton) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_downLoadButton];
    
    [self updateTollbarState];
}

- (void)createBottomView
{
    _bottomView = [[UIView alloc] init];
    [_bottomView setFrame:CGRectMake(0,self.view.bounds.size.height - PLAYER_CONTROL_BAR_HEIGHT,self.view.bounds.size.width,PLAYER_CONTROL_BAR_HEIGHT)];
    [_bottomView setOpaque:NO];
    [_bottomView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    [self.view addSubview:_bottomView];
    
    _previousButton = [[UIButton alloc] init];
    _previousButton.frame = CGRectMake(80, 2, 40, 40);
    [_previousButton setImage:[UIImage imageNamed:@"left_arrow"] forState:UIControlStateNormal];
    [_previousButton setShowsTouchWhenHighlighted:YES];
    [_previousButton addTarget:self action:@selector(clickPreviousButton) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:_previousButton];
    
    _nextButton = [[UIButton alloc] init];
    _nextButton.frame = CGRectMake(320-80-40, 2, 40, 40);
    [_nextButton setImage:[UIImage imageNamed:@"right_arrow"] forState:UIControlStateNormal];
    [_nextButton addTarget:self action:@selector(clickNextButton) forControlEvents:UIControlEventTouchUpInside];
    [_nextButton setShowsTouchWhenHighlighted:YES];
    [_bottomView addSubview:_nextButton];
    
    if (self.photos.count <= 1) {
        _previousButton.enabled = NO;
        _nextButton.enabled = NO;
    }
}

#pragma mark 创建UIScrollView
- (void)createScrollView
{
    CGRect frame = self.view.bounds;
    frame.origin.x -= kPadding;
    frame.size.width += (2 * kPadding);
	_photoScrollView = [[UIScrollView alloc] initWithFrame:frame];
	_photoScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_photoScrollView.pagingEnabled = YES;
	_photoScrollView.delegate = self;
	_photoScrollView.showsHorizontalScrollIndicator = NO;
	_photoScrollView.showsVerticalScrollIndicator = NO;
	_photoScrollView.backgroundColor = [UIColor clearColor];
    _photoScrollView.contentSize = CGSizeMake(frame.size.width * _photos.count, 0);
	[self.view addSubview:_photoScrollView];
    _photoScrollView.contentOffset = CGPointMake(_currentPhotoIndex * frame.size.width, 0);
}

- (void)setPhotos:(NSArray *)photos
{
    _photos = photos;
    
    if (photos.count > 1) {
        _visiblePhotoViews = [NSMutableSet set];
        _reusablePhotoViews = [NSMutableSet set];
    }
    
    for (int i = 0; i<_photos.count; i++) {
        MJPhoto *photo = _photos[i];
        photo.index = i;
        photo.firstShow = i == _currentPhotoIndex;
    }
}

#pragma mark - Action

- (void)clickCloseButton
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)clickDownLoadButton
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        MJPhoto *photo = _photos[self.currentPhotoIndex];
        UIImageWriteToSavedPhotosAlbum(photo.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    });
}

- (void)clickPreviousButton
{

}

- (void)clickNextButton
{
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    if (error) {
        [MBProgressHUD showError:@"Failed" toView:nil];
    } else {
        MJPhoto *photo = _photos[_currentPhotoIndex];
        photo.save = YES;
        _downLoadButton.enabled = NO;
        [MBProgressHUD showSuccess:@"Completed" toView:nil];
    }
}

#pragma mark - 设置选中的图片
- (void)setCurrentPhotoIndex:(NSUInteger)currentPhotoIndex
{
    _currentPhotoIndex = currentPhotoIndex;
    
    for (int i = 0; i<_photos.count; i++) {
        MJPhoto *photo = _photos[i];
        photo.firstShow = i == currentPhotoIndex;
    }
    
    if ([self isViewLoaded]) {
        _photoScrollView.contentOffset = CGPointMake(_currentPhotoIndex * _photoScrollView.frame.size.width, 0);
        
        // 显示所有的相片
        [self showPhotos];
    }
}

#pragma mark - MJPhotoView代理

- (void)photoViewSingleTap:(MJPhotoView *)photoView
{
    isControlViewHidden = !isControlViewHidden;
    
    [UIView animateWithDuration:0.4 animations:^{
        _topView.hidden = isControlViewHidden;
        _bottomView.hidden = isControlViewHidden;
    } completion:nil];
}

- (void)photoViewDidEndZoom:(MJPhotoView *)photoView
{
   
}

- (void)photoViewImageFinishLoad:(MJPhotoView *)photoView
{
//    _toolbar.currentPhotoIndex = _currentPhotoIndex;
}

#pragma mark 显示照片
- (void)showPhotos
{
    // 只有一张图片
    if (_photos.count == 1) {
        [self showPhotoViewAtIndex:0];
        return;
    }
    
    CGRect visibleBounds = _photoScrollView.bounds;
	int firstIndex = (int)floorf((CGRectGetMinX(visibleBounds)+kPadding*2) / CGRectGetWidth(visibleBounds));
	int lastIndex  = (int)floorf((CGRectGetMaxX(visibleBounds)-kPadding*2-1) / CGRectGetWidth(visibleBounds));
    if (firstIndex < 0) firstIndex = 0;
    if (firstIndex >= _photos.count) firstIndex = _photos.count - 1;
    if (lastIndex < 0) lastIndex = 0;
    if (lastIndex >= _photos.count) lastIndex = _photos.count - 1;
	
	// 回收不再显示的ImageView
    NSInteger photoViewIndex;
	for (MJPhotoView *photoView in _visiblePhotoViews) {
        photoViewIndex = kPhotoViewIndex(photoView);
		if (photoViewIndex < firstIndex || photoViewIndex > lastIndex) {
			[_reusablePhotoViews addObject:photoView];
			[photoView removeFromSuperview];
		}
	}
    
	[_visiblePhotoViews minusSet:_reusablePhotoViews];
    while (_reusablePhotoViews.count > 2) {
        [_reusablePhotoViews removeObject:[_reusablePhotoViews anyObject]];
    }
	
	for (NSUInteger index = firstIndex; index <= lastIndex; index++) {
		if (![self isShowingPhotoViewAtIndex:index]) {
			[self showPhotoViewAtIndex:index];
		}
	}
}

#pragma mark 显示一个图片view
- (void)showPhotoViewAtIndex:(int)index
{
    MJPhotoView *photoView = [self dequeueReusablePhotoView];
    if (!photoView) { // 添加新的图片view
        photoView = [[MJPhotoView alloc] init];
        photoView.photoViewDelegate = self;
    }
    
    // 调整当期页的frame
    CGRect bounds = _photoScrollView.bounds;
    CGRect photoViewFrame = bounds;
    photoViewFrame.size.width -= (2 * kPadding);
    photoViewFrame.origin.x = (bounds.size.width * index) + kPadding;
    photoView.tag = kPhotoViewTagOffset + index;
    
    MJPhoto *photo = _photos[index];
    photoView.frame = photoViewFrame;
    photoView.photo = photo;
    
    [_visiblePhotoViews addObject:photoView];
    [_photoScrollView addSubview:photoView];
    
    [self loadImageNearIndex:index];
}

#pragma mark 加载index附近的图片
- (void)loadImageNearIndex:(int)index
{
    if (index > 0) {
        MJPhoto *photo = _photos[index - 1];
        [[SDWebImageManager sharedManager] downloadWithURL:photo.url options:0 progress:nil completed:nil];
    }
    
    if (index < _photos.count - 1) {
        MJPhoto *photo = _photos[index + 1];
        [[SDWebImageManager sharedManager] downloadWithURL:photo.url options:0 progress:nil completed:nil];
    }
}

#pragma mark index这页是否正在显示
- (BOOL)isShowingPhotoViewAtIndex:(NSUInteger)index {
	for (MJPhotoView *photoView in _visiblePhotoViews) {
		if (kPhotoViewIndex(photoView) == index) {
           return YES;
        }
    }
	return  NO;
}

#pragma mark 循环利用某个view
- (MJPhotoView *)dequeueReusablePhotoView
{
    MJPhotoView *photoView = [_reusablePhotoViews anyObject];
	if (photoView) {
		[_reusablePhotoViews removeObject:photoView];
	}
	return photoView;
}

#pragma mark 更新toolbar状态
- (void)updateTollbarState
{
    _currentPhotoIndex = _photoScrollView.contentOffset.x / _photoScrollView.frame.size.width;
    
    MJPhoto *photo =(MJPhoto *)[self.photos objectAtIndex:self.currentPhotoIndex];
    NSString *title = [photo.url lastPathComponent];
    _titleLabel.text = title;
    
    if (self.currentPhotoIndex == 0) {
        _previousButton.enabled = NO;
    }else if (self.currentPhotoIndex == self.photos.count-1){
        _nextButton.enabled = NO;
    }
    
    _downLoadButton.enabled = photo.image != nil && !photo.save;
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	  [self showPhotos];
    [self updateTollbarState];
}
@end