//
//  ZJCameraListVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-10-24.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ZJPhotoVC.h"
#import "ZJVideoVC.h"

@interface ZJCameraListVC : UIViewController<UIScrollViewDelegate>
{
    BOOL pageControlUsed;
}

@property (nonatomic, strong) NSMutableArray *photoArr;
@property (nonatomic, strong) NSMutableArray *videoArr;

@property (strong, nonatomic) UIScrollView *contentView;

@property (strong, nonatomic) ZJPhotoVC *photoVC;
@property (strong, nonatomic) ZJVideoVC *videoVC;
@end
