//
//  ZJSetttingVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "MBProgressHUD+Add.h"
#import "TFHpple.h"
#import "ZJInterfaceEntry.h"
#import "SDImageCache.h"
#import "ZJConfigClass.h"

@interface ZJSetttingVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,ZJConfigDelegate>
{
}

@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) ZJConfigClass *config;
@end
