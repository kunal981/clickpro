//
//  ZJAppDelegate.m
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJAppDelegate.h"
#import "ZJGuideVC.h"
#import "ZJHomeVC.h"
#import "MBProgressHUD.h"

@implementation ZJAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    sleep(3);
    
    NSError *setCategoryErr = nil;
    NSError *activationErr  = nil;
    [[AVAudioSession sharedInstance]
     setCategory: AVAudioSessionCategoryPlayback
     error: &setCategoryErr];
    [[AVAudioSession sharedInstance]
     setActive: YES
     error: &activationErr];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor blackColor];
    
    self.homeVC = [[ZJHomeVC alloc] init];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.homeVC];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bg"] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    self.window.rootViewController = self.navigationController;
    
    [self.window makeKeyAndVisible];
        
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];

        [self showGuide];
    }
    
    [self connectCamera];
    
    return YES;
}

- (void)showGuide
{
    ZJGuideVC *guideVC = [[ZJGuideVC alloc] init];
    [self.window.rootViewController presentViewController:guideVC animated:NO completion:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    /*
    UIApplication*   app = [UIApplication sharedApplication];
    __block    UIBackgroundTaskIdentifier bgTask;
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (bgTask != UIBackgroundTaskInvalid)
            {
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    }];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (bgTask != UIBackgroundTaskInvalid)
            {
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    });
     */
    
    [self disConnectCamera];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self connectCamera];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - data

- (void)connectCamera
{
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(didFinished:);
    interface.didFinishedError = @selector(didFinishedError);
    [interface connectCamera];
}

- (void)didFinished:(id)data
{
//    [self openStream];

    [MBProgressHUD showHUDAddedTo:self.window animated:YES];
    [[ZJConfigClass sharedInstance] getCofigSetting:self.homeVC];
}

- (void)didFinishedError
{
    
}

- (void)openStream
{
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(openStreamFinished:);
    interface.didFinishedError = @selector(openStreamFinishedError);
    [interface openStream];
}

- (void)openStreamFinished:(id)data
{
    [self setStreamType];
}

- (void)openStreamFinishedError
{
    [self setStreamType];
}


- (void)setStreamType
{
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(setStreamTypeFinished:);
    interface.didFinishedError = @selector(setStreamTypeFinishedError);
    [interface setStreamType];
}

- (void)setStreamTypeFinished:(id)data
{
}

- (void)setStreamTypeFinishedError
{
}

- (void)disConnectCamera
{
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(disConnectCameraFinished:);
    interface.didFinishedError = @selector(disConnectCameraFinishedError);
    [interface disConnectCamera];
}

- (void)disConnectCameraFinished:(id)data
{
}

- (void)disConnectCameraFinishedError
{
    
}

@end
