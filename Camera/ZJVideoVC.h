//
//  ZJPhotoVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-11-7.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UINavigationController+Rotation.h"

@interface ZJVideoVC : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *videoArr;
@property (nonatomic, strong) UITableView *tableView;

@end
