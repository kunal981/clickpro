//
//  ZJSetttingVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJConfigListVC.h"

@interface ZJConfigListVC ()
{
}
@end

@implementation ZJConfigListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = RGBACOLOR(236, 236, 236, 1);
    
    configIndex = self.currentIndex;
    
    [self uploadNav];
    [self uploadView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)uploadNav
{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 29, 29);
    [backButton setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(clickBackButton) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.title = [self.configDic objectForKey:@"title"];
}

- (void)uploadView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 0, 300, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 12)];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = tableHeaderView;
    
    UIView *tableFooterView = [[UIView alloc] init];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = tableFooterView;
    
    [self.view addSubview:self.tableView];
}

#pragma mark - Action

- (void)clickBackButton
{
    if (configIndex == self.currentIndex) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:ZJLocalizedString(@"Save settings") delegate:self cancelButtonTitle:nil otherButtonTitles:ZJLocalizedString(@"YES"),ZJLocalizedString(@"NO"), nil];
    [alert show];
}

#pragma mark - Data

- (void)readyToConfig
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(readyToConfigFinished:);
    interface.didFinishedError = @selector(readyToConfigFinishedError);
    [interface readyToConfig];
}

- (void)readyToConfigFinished:(id)data
{
    [self setConfig:self.currentIndex];
}

- (void)readyToConfigFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showError:ZJLocalizedString(@"Failed") toView:nil];
}

- (void)setConfig:(NSInteger)index
{
    
    NSString *param = [self.configDic objectForKey:@"param"];
    NSString *value = [self.titleArr objectAtIndex:index];
    
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(setConfigFinished:);
    interface.didFinishedError = @selector(setConfigFinishedError);
    [interface setConfigWithParam:param Value:value];
}

- (void)setConfigFinished:(id)data
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showSuccess:ZJLocalizedString(@"Completed") toView:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    if (_delegate && [_delegate respondsToSelector:@selector(zjConfigListVC:didSuccessConfigWithConfigTye:SettingIndex:)]) {
        
        [_delegate zjConfigListVC:self didSuccessConfigWithConfigTye:[self.titleArr objectAtIndex:self.currentIndex] SettingIndex:self.settingIndex];
    }
}

- (void)setConfigFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showError:ZJLocalizedString(@"Failed") toView:nil];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self readyToConfig];
    }else{
    
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textColor = RGBACOLOR(28, 28, 28, 1);
        cell.textLabel.highlightedTextColor = RGBACOLOR(28, 28, 28, 1);
        
        cell.detailTextLabel.font = [UIFont systemFontOfSize:13];
        cell.detailTextLabel.textColor = RGBACOLOR(165, 165, 165, 1);
        cell.detailTextLabel.highlightedTextColor = RGBACOLOR(165, 165, 165, 1);
        
        UIImageView *accessory = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 21, 21)];
        cell.accessoryView = accessory;
    }
    
    cell.textLabel.text = [self.titleArr objectAtIndex:indexPath.row];

    if (self.currentIndex == indexPath.row) {
        
        [(UIImageView *)cell.accessoryView setImage:[UIImage imageNamed:@"icon_selected"]];
        
    } else {
        [(UIImageView *)cell.accessoryView setImage:[UIImage imageNamed:@"icon_unselected"]];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSIndexPath *lastIndex = [NSIndexPath indexPathForRow:self.currentIndex inSection:0];
    UITableViewCell *lastCell = [tableView cellForRowAtIndexPath:lastIndex];
    [(UIImageView *)lastCell.accessoryView setImage:[UIImage imageNamed:@"icon_unselected"]];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [(UIImageView *)cell.accessoryView setImage:[UIImage imageNamed:@"icon_selected"]];
    
    self.currentIndex = indexPath.row;
}
@end
