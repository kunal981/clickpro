//
//  ZJHomeVC.m
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJHomeVC.h"
#import "ZJCameraListVC.h"
#import "ZJSetttingVC.h"
#import "ZJInterfaceEntry.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+Add.h"

#define BATTERY_STATUS_LENGTH 13

#define STREAM_PATH [[[NSHomeDirectory()   stringByAppendingPathComponent:@"Library"] stringByAppendingPathComponent:@"Caches"] stringByAppendingPathComponent:@"Stream"]


@interface ZJHomeVC ()
@property (nonatomic, strong) UIImageView *batteryStatusImageView;
@property (nonatomic, strong) UILabel *batteryStatusLabel;
@property (nonatomic, strong) UIButton *startButton;
@property (nonatomic, strong) UIView *recordTimeView;
@property (nonatomic, strong) UIImageView *redPointImageView;
@property (nonatomic, strong) UIView *modeView;

@end

@implementation ZJHomeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = RGBACOLOR(35, 35, 35, 1);
    
    if (IOS_VERSION >= 7.0) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        
        statusHeight = 20;
    }

    recordTotalTime = 0;
    recordTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateRecordTime) userInfo:nil repeats:YES];
    [recordTimer setFireDate:[NSDate distantFuture]];
    
    self.modeButtons = [NSMutableArray array];
    self.oldStreamKey = [NSString string];
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager isExecutableFileAtPath:STREAM_PATH]) {
        //创建VideoImageFile文件夹
        [fileManager createDirectoryAtPath:STREAM_PATH withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    if (![fileManager isExecutableFileAtPath:[STREAM_PATH stringByAppendingPathComponent:@"Temp"]]) {
        [fileManager createDirectoryAtPath:[STREAM_PATH stringByAppendingPathComponent:@"Temp"] withIntermediateDirectories:YES attributes:nil error:nil];
    }

    [self uploadNav];
    [self uploadVideoView];
    [self uploadBottomView];
    [self uploadModeView];
    
    self.config = [ZJConfigClass sharedInstance];
//    [self.config getCofigSetting:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (!isLoading) {
        isLoading = YES;
        [self reloadStream];
        [self getConfigFinished];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    isLoading = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)uploadNav
{
    /*
    UIView *batteryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
    batteryView.backgroundColor = [UIColor clearColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = [UIImage imageNamed:@"icon_battery"];
    [batteryView addSubview:imageView];
    
    self.batteryStatusImageView = [[UIImageView alloc] init];
    self.batteryStatusImageView.frame = CGRectMake(7, 11, BATTERY_STATUS_LENGTH*0.3, 8);
    self.batteryStatusImageView.backgroundColor = RGBACOLOR(187, 187, 187, 1);
    [imageView addSubview:self.batteryStatusImageView];
    
    self.batteryStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 50, 30)];
    self.batteryStatusLabel.text = @"30%";
    self.batteryStatusLabel.textColor = RGBACOLOR(204, 204, 204, 1);
    self.batteryStatusLabel.font = [UIFont systemFontOfSize:17];
    self.batteryStatusLabel.backgroundColor = [UIColor clearColor];
    [batteryView addSubview:self.batteryStatusLabel];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:batteryView];
     */
    
    
    UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    refreshButton.frame = CGRectMake(0, 0, 29, 29);
    [refreshButton setImage:[UIImage imageNamed:@"icon_refresh"] forState:UIControlStateNormal];
    [refreshButton addTarget:self action:@selector(clickRefreshButton) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:refreshButton];
   
    
    self.videoResolutionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.videoResolutionBtn.frame = CGRectMake(0, 0, 240, 30);
    [self.videoResolutionBtn setImage:[UIImage imageNamed:@"icon_camera"] forState:UIControlStateNormal];
    self.videoResolutionBtn.titleLabel.font = [UIFont systemFontOfSize:17];
//    self.videoResolutionBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.videoResolutionBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [self.videoResolutionBtn setTitle:self.config.photoSize forState:UIControlStateNormal];
    [self.videoResolutionBtn setTitleColor:RGBACOLOR(204, 204, 204, 1) forState:UIControlStateNormal];
    self.videoResolutionBtn.userInteractionEnabled = NO;
    self.navigationItem.titleView = self.videoResolutionBtn;
    
    UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.frame = CGRectMake(0, 0, 29, 29);
    [settingButton setImage:[UIImage imageNamed:@"btn_setting"] forState:UIControlStateNormal];
    [settingButton addTarget:self action:@selector(clickSettingButton) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:settingButton];
}

- (void)uploadVideoView
{
    UIView *videoView = [[UIView alloc] init];
    videoView.frame =  CGRectMake(0, 0, 320, self.view.frame.size.height-44-statusHeight-80);
    videoView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:videoView];
    
//    self.totalTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 15)];
//    self.totalTimeLabel.text = @"10:02:15";
//    self.totalTimeLabel.textColor = RGBACOLOR(204, 204, 204, 1);
//    self.totalTimeLabel.backgroundColor = [UIColor clearColor];
//    [videoView addSubview:self.totalTimeLabel];
    
    self.recordTimeView = [[UIView alloc] initWithFrame:CGRectMake(225, 0, 85, 15)];
    [videoView addSubview:self.recordTimeView];
    
    self.redPointImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, videoView.frame.size.height/2-120-20, 10, 15)];
    self.redPointImageView.contentMode = UIViewContentModeCenter;
    self.redPointImageView.animationImages=[NSArray arrayWithObjects:
                             [UIImage imageNamed:@"icon_redpoint"],
                             [UIImage imageNamed:@"icon_transparentPoint"],nil ];
    
    //设置动画总时间
    self.redPointImageView.animationDuration = 1.0;
    self.redPointImageView.animationRepeatCount = 0;//设置动画次数 0 表示无限
    [self.recordTimeView addSubview:self.redPointImageView];
    
    self.recordTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, videoView.frame.size.height/2-120-20, 80, 15)];
    self.recordTimeLabel.text = @"00:00:00";
    self.recordTimeLabel.textAlignment = NSTextAlignmentRight;
    self.recordTimeLabel.textColor = RGBACOLOR(204, 204, 204, 1);
    self.recordTimeLabel.backgroundColor = [UIColor clearColor];
    [self.recordTimeView addSubview:self.recordTimeLabel];
    
    self.streamImageView = [[UIImageView alloc] init];
    self.streamImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.streamImageView.bounds = CGRectMake(0, 0, 320, 240);
    self.streamImageView.center = CGPointMake(160, videoView.frame.size.height/2);
    [self.streamImageView setBackgroundColor:[UIColor clearColor]];
    self.streamImageView.userInteractionEnabled = YES;
    [videoView addSubview:self.streamImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resetVF)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self.streamImageView addGestureRecognizer:tap];
}

- (void)uploadModeView
{
    self.modeView = [[UIView alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height-145-44-statusHeight, 300, 56)];
    self.modeView.backgroundColor = RGBACOLOR(16, 16, 16, 1);
    self.modeView.hidden = YES;
    [self.view addSubview:self.modeView];
    
    int count = 2;
    
    for (int i = 0; i<count; i++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(self.modeView.frame.size.width/count*i, 0, self.modeView.frame.size.width/count, 56);
        button.tag = i;
        if (i == 0) {
            [button setImage:[UIImage imageNamed:@"btn_camera"] forState:UIControlStateNormal];
        }
//        else if (i == 1){
//            [button setImage:[UIImage imageNamed:@"btn_photolibrary"] forState:UIControlStateNormal];
//        }
        else{
            [button setImage:[UIImage imageNamed:@"btn_videocamera"] forState:UIControlStateNormal];
        }
        
        [button addTarget:self action:@selector(clickModeButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.modeView addSubview:button];
        
        [self.modeButtons addObject:button];
        
        if (i < 1) {

            UIImageView *line = [[UIImageView alloc] init];
            line.frame = CGRectMake(self.modeView.frame.size.width/count+self.modeView.frame.size.width/count*i, 10, 1, 36);
            line.image = [UIImage imageNamed:@"line"];
            [self.modeView addSubview:line];
        }
    }
    
    [self clickModeButton:[self.modeButtons objectAtIndex:1]];
}

- (void)uploadBottomView
{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-80-44-statusHeight, 320, 80)];
    bottomView.backgroundColor = RGBACOLOR(23, 23, 23, 1);
    [self.view addSubview:bottomView];
    
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame = CGRectMake(20-6, 25-6, 42, 42);
    [playButton setImage:[UIImage imageNamed:@"icon_play"] forState:UIControlStateNormal];
    [playButton addTarget:self action:@selector(clickPlayButton) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:playButton];
    
    self.startButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.startButton.bounds = CGRectMake(0, 0, 58, 58);
    self.startButton.center = CGPointMake(160, 40);
    [self.startButton addTarget:self action:@selector(clickStartButton) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:self.startButton];

    UIButton *modeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    modeButton.frame = CGRectMake(self.view.frame.size.width-50-6, 25-6, 42, 42);
    [modeButton setImage:[UIImage imageNamed:@"btn_mode"] forState:UIControlStateNormal];
    [modeButton addTarget:self action:@selector(clickControlModeButton) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:modeButton];
}

- (void)updateRecordTimeLabel
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:recordTotalTime];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setDateFormat:@"HH:mm:ss"];

    self.recordTimeLabel.text = [formatter stringFromDate:date];
}

#pragma mark - Action

- (void)clickRefreshButton
{
    [self connectCamera];
}

- (void)clickSettingButton
{
    if (sessionFlag == Session_TakeVideo_Stop) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:ZJLocalizedString(@"Please stop recording!") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    ZJSetttingVC *settingVC = [[ZJSetttingVC alloc] init];
    [self.navigationController pushViewController:settingVC animated:YES];
}

- (void)clickModeButton:(UIButton *)button
{
    self.modeView.hidden = YES;
    
    if (sessionFlag == Session_TakeVideo_Stop) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:ZJLocalizedString(@"Please stop recording!") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    [self dimAllButtonExceptButton:button];
    
    if(button.tag == 0){
    
        sessionFlag = Session_TakePhoto;
        
        if ([self.config.burstMode intValue] == 0) {
            
            [self.videoResolutionBtn setTitle:self.config.photoSize forState:UIControlStateNormal];
            
            [self.videoResolutionBtn setImage:[UIImage imageNamed:@"icon_camera"] forState:UIControlStateNormal];
            
            [self.startButton setImage:[UIImage imageNamed:@"btn_takePhoto"] forState:UIControlStateNormal];
        }else{
            [self.videoResolutionBtn setTitle:[NSString stringWithFormat:@"%@ photos",self.config.burstMode] forState:UIControlStateNormal];
            
            [self.videoResolutionBtn setImage:[UIImage imageNamed:@"icon_lianpai"] forState:UIControlStateNormal];
            [self.startButton setImage:[UIImage imageNamed:@"btn_continuous"] forState:UIControlStateNormal];
        }
    }else {
    
        sessionFlag = Session_TakeVideo_Start;
        [self.videoResolutionBtn setTitle:self.config.videoResolution forState:UIControlStateNormal];
        [self.videoResolutionBtn setImage:[UIImage imageNamed:@"icon_video"] forState:UIControlStateNormal];
        [self.startButton setImage:[UIImage imageNamed:@"btn_takeVideo_start"] forState:UIControlStateNormal];
    }
    
    if (button.tag == 0) {
        self.recordTimeView.hidden = YES;
    }else{
        self.recordTimeView.hidden = NO;
    }
}

- (void)dimAllButtonExceptButton:(UIButton *)selectedButton
{
    for (int i = 0; i < self.modeButtons.count; i++) {
        UIButton *button = (UIButton *)[self.modeButtons objectAtIndex:i];
        if (button == selectedButton) {
            button.enabled = NO;
        }else{
            button.enabled = YES;
        }
    }
}

- (void)clickPlayButton
{
    if (sessionFlag == Session_TakeVideo_Stop) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:ZJLocalizedString(@"Please stop recording!") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    ZJCameraListVC *cameraList = [[ZJCameraListVC alloc] init];
    [self.navigationController pushViewController:cameraList animated:YES];
}

- (void)clickStartButton
{
    if (sessionFlag == Session_TakePhoto) {
        
        [self takePhoto];
        
    }else if (sessionFlag == Session_TakeVideo_Start){
        
        [self.startButton setImage:[UIImage imageNamed:@"btn_takeVideo_stop"] forState:UIControlStateNormal];
        
        [self startRecord];
        
    }else if (sessionFlag == Session_TakeVideo_Stop){
    
        [self.startButton setImage:[UIImage imageNamed:@"btn_takeVideo_start"] forState:UIControlStateNormal];
        [self stopRecord];
    }
}

- (void)clickControlModeButton
{
    CATransition *animation = [CATransition animation];
    animation.duration = 0.2f;
    animation.type = kCATransitionFade;
    self.modeView.hidden = !self.modeView.hidden;
    [self.modeView.layer addAnimation:animation forKey:@"animation"];
}

- (void)reloadStream
{
    if (isLoading) {

        NSTimeInterval aInterval =[[NSDate date] timeIntervalSince1970];
        NSString *stream = [NSString stringWithFormat:@"http://192.168.42.1/mjpeg/amba.jpg?ts=%0.0f",aInterval*1000];
        
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:stream] options:0 progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){

            UIImage *streamImage = [UIImage imageWithData:data];
            [self.streamImageView setImage:streamImage];
            [self reloadStream];
        }];
    }else{
        self.streamImageView.image = nil;
    }
}

- (void)updateRecordTime
{
    recordTotalTime++;
    
    [self updateRecordTimeLabel];
}

#pragma mark - Data

- (void)connectCamera
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Connecting...";
    
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(didFinished:);
    interface.didFinishedError = @selector(didFinishedError);
    [interface connectCamera];
}

- (void)didFinished:(id)data
{
    [self openStream];
}

- (void)didFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)openStream
{
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(openStreamFinished:);
    interface.didFinishedError = @selector(openStreamFinishedError);
    [interface openStream];
}

- (void)openStreamFinished:(id)data
{
    [self setStreamType];
}

- (void)openStreamFinishedError
{
    [self setStreamType];
}


- (void)setStreamType
{
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(setStreamTypeFinished:);
    interface.didFinishedError = @selector(setStreamTypeFinishedError);
    [interface setStreamType];
}

- (void)setStreamTypeFinished:(id)data
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)setStreamTypeFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)resetVF
{
    if (sessionFlag == Session_TakeVideo_Stop) {
        return;
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Resetting...";
    
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(resetVFFinished:);
    interface.didFinishedError = @selector(resetVFFinishedError);
    [interface resetVF];
}

- (void)resetVFFinished:(id)data
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (!isLoading) {
        isLoading = YES;
        [self reloadStream];
    }
}

- (void)resetVFFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)takePhoto
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(takePhotoFinished:);
    interface.didFinishedError = @selector(takePhotoFinishedError);
    [interface takePhoto];
}

- (void)takePhotoFinished:(id)data
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showSuccess:ZJLocalizedString(@"Completed") toView:nil];
}

- (void)takePhotoFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showError:ZJLocalizedString(@"Failed") toView:nil];
}

- (void)startRecord
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(startRecordFinished:);
    interface.didFinishedError = @selector(startRecordFinishedError);
    [interface startRecord];
}

- (void)startRecordFinished:(id)data
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    recordTotalTime = 0;
    [recordTimer setFireDate:[NSDate date]];
    
    [self.redPointImageView startAnimating];

    sessionFlag = Session_TakeVideo_Stop;
}

- (void)startRecordFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showError:ZJLocalizedString(@"Failed") toView:nil];
}

- (void)stopRecord
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(stopRecordFinished:);
    interface.didFinishedError = @selector(stopRecordFinishedError);
    [interface stopRecord];
}

- (void)stopRecordFinished:(id)data
{
    [self.redPointImageView stopAnimating];

    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showSuccess:ZJLocalizedString(@"Completed") toView:nil];
    
    recordTotalTime = 0;
    [recordTimer setFireDate:[NSDate distantFuture]];
    [self updateRecordTimeLabel];
    
    sessionFlag = Session_TakeVideo_Start;
}

- (void)stopRecordFinishedError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showError:ZJLocalizedString(@"Failed") toView:nil];
}

#pragma mark - ZJConfigDelegate

- (void)getConfigFinished
{
    [MBProgressHUD hideHUDForView:self.view.window animated:YES];

    if(sessionFlag == Session_TakePhoto){
    
        if ([self.config.burstMode intValue] == 0) {
            
            [self.videoResolutionBtn setTitle:self.config.photoSize forState:UIControlStateNormal];
            
            [self.videoResolutionBtn setImage:[UIImage imageNamed:@"icon_camera"] forState:UIControlStateNormal];
            
            [self.startButton setImage:[UIImage imageNamed:@"btn_takePhoto"] forState:UIControlStateNormal];
        }else{
            [self.videoResolutionBtn setTitle:[NSString stringWithFormat:@"%@ photos",self.config.burstMode] forState:UIControlStateNormal];
            
            [self.videoResolutionBtn setImage:[UIImage imageNamed:@"icon_lianpai"] forState:UIControlStateNormal];
            [self.startButton setImage:[UIImage imageNamed:@"btn_continuous"] forState:UIControlStateNormal];
        }
    }else {
        [self.videoResolutionBtn setTitle:self.config.videoResolution forState:UIControlStateNormal];
    }
    
    UIButton *modeButton = (UIButton *)[self.modeButtons objectAtIndex:0];
    
    if ([self.config.burstMode intValue] == 0) {
        [modeButton setImage:[UIImage imageNamed:@"btn_camera"] forState:UIControlStateNormal];
    }else{
        [modeButton setImage:[UIImage imageNamed:@"btn_photolibrary"] forState:UIControlStateNormal];
    }
}

@end
