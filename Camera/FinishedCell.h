//
//  FinishedCell.h


#import <UIKit/UIKit.h>
#import "FileModel.h"

@interface FinishedCell : UITableViewCell {
    FileModel *fileInfo;

}
@property(nonatomic,assign)UIViewController *delegate;
@property(nonatomic,retain) FileModel *fileInfo;
@property(nonatomic,retain)IBOutlet UILabel *fileName;
@property(nonatomic,retain)IBOutlet UILabel *fileSize;
@property (retain, nonatomic) IBOutlet UILabel *timelable;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;


- (IBAction)deleteFile:(id)sender;
- (IBAction)openFile:(UIButton *)sender;


@end
