//
//  UINavigationController+Rotation.m
//  GuMei
//
//  Created by Apple_ZJ on 13-6-18.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "UINavigationController+Rotation.h"

@implementation UINavigationController (Rotation)

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return self.topViewController.shouldAutorotate;
}

- (BOOL)shouldAutorotate
{
    return self.topViewController.shouldAutorotate;
}

- (NSUInteger)supportedInterfaceOrientations
{

    return self.topViewController.supportedInterfaceOrientations;
}

@end
