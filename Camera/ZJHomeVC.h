//
//  ZJHomeVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFHpple.h"
#import "SDWebImageDownloader.h"
#import "ZJConfigClass.h"

@interface ZJHomeVC : UIViewController<ZJConfigDelegate>
{    
    BOOL isLoading;
    
    Session sessionFlag;
    
    int recordTotalTime;
    NSTimer *recordTimer;
    
    float statusHeight;
}

@property (nonatomic, strong) UILabel *totalTimeLabel;
@property (nonatomic, strong) UILabel *recordTimeLabel;
@property (nonatomic, strong) UIButton *videoResolutionBtn;
@property (nonatomic, strong) UIImageView *streamImageView;
@property (nonatomic, strong) NSMutableArray *modeButtons;
@property (nonatomic, strong) NSString *oldStreamKey;

@property (nonatomic, strong) ZJConfigClass *config;

- (void)reloadStream;
@end
