/* Copyright (C) 2012 IGN Entertainment, Inc. */

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ZJVideoPlayView: UIView {
}

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

- (void)setPlayer:(AVPlayer *)player;

@end
