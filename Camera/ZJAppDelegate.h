//
//  ZJAppDelegate.h
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJInterfaceEntry.h"
#import <AVFoundation/AVFoundation.h>

@class HTTPServer;
@class ZJHomeVC;

@interface ZJAppDelegate : UIResponder <UIApplicationDelegate>
{
    Session sessionFlag;
    
    HTTPServer *httpServer;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) ZJHomeVC *homeVC;

- (void)showGuide;
@end
