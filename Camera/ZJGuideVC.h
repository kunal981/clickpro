//
//  ZJGuideVC.h
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMPageControl.h"
#import <QuartzCore/QuartzCore.h>

@interface ZJGuideVC : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) SMPageControl *pageControl;
@end
