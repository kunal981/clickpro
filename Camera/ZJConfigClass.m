//
//  ZJConfigClass.m
//  Camera
//
//  Created by Apple_ZJ on 13-11-29.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import "ZJConfigClass.h"

@implementation ZJConfigClass

+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
}

#pragma mark - Data

- (void)getCofigSetting:(id)dele
{
    self.delegate = dele;
    
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(getVideoSettingFinished:);
    interface.didFinishedError = @selector(getVideoSettingFinishedError);
    [interface getSetting:VIDEOTAB];
}

- (void)getVideoSettingFinished:(id)data
{
    //videoResolution
    NSData *videoResolutionData = [self getSubData:data RangStartStr:@"<p>\"video resolution\"</p>" RangEndStr:@"<p>\"video quality\"</p>"];
    TFHpple *videoResolutionXpathParser = [[TFHpple alloc] initWithHTMLData:videoResolutionData];
    
    NSArray *videoResolutionElements = [videoResolutionXpathParser searchWithXPathQuery:@"//option"];
    
    if (videoResolutionElements.count) {
        
        [self.videoResolutionArr removeAllObjects];
        
        for (int i = 0;i < videoResolutionElements.count;i++) {
            
            TFHppleElement *element = [videoResolutionElements objectAtIndex:i];
            
            NSString *value = [[[element firstChild] content] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            if (i == 0) {
                
                self.videoResolution = value;
            }else{
                
                if (value) {
                    [self.videoResolutionArr addObject:value];
                }
            }
        }
    }
    
    //dual streams
    NSData *dualStreamsData = [self getSubData:data RangStartStr:@"<p>\"dual streams\"</p>" RangEndStr:@"<p>\"stream type\"</p>"];
    TFHpple *dualStreamsXpathParser = [[TFHpple alloc] initWithHTMLData:dualStreamsData];
    
    NSArray *dualStreamsElements = [dualStreamsXpathParser searchWithXPathQuery:@"//option"];
    
    if (dualStreamsElements.count) {
        
        [self.dualStreamsArr removeAllObjects];
        
        for (int i = 0;i < dualStreamsElements.count;i++) {
            
            TFHppleElement *element = [dualStreamsElements objectAtIndex:i];
            
            NSString *value = [[[element firstChild] content] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            if (i == 0) {
                
                self.dualStreams = value;
            }else{
                
                if (value) {
                    [self.dualStreamsArr addObject:value];
                }
            }
        }
    }
    
    //VideoStamp
    NSData *videoStampData = [self getSubData:data RangStartStr:@"<p>\"video stamp\"</p>" RangEndStr:@"</body>"];
    TFHpple *videoStampXpathParser = [[TFHpple alloc] initWithHTMLData:videoStampData];
    
    NSArray *videoStampElements = [videoStampXpathParser searchWithXPathQuery:@"//option"];
    
    if (videoStampElements.count) {
        
        [self.videoStampArr removeAllObjects];
        
        for (int i = 0;i < videoStampElements.count;i++) {
            
            TFHppleElement *element = [videoStampElements objectAtIndex:i];
            
            NSString *value = [[[element firstChild] content] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            if (i == 0) {
                
                self.videoStamp = value;
                
            }else{
                
                if (value) {
                    [self.videoStampArr addObject:value];
                }
            }
        }
    }
    
    [self getPhotoSetting];
}

- (void)getVideoSettingFinishedError
{
    [self getPhotoSetting];
}

- (void)getPhotoSetting
{
    ZJInterfaceEntry *interface = [[ZJInterfaceEntry alloc] init];
    interface.delegate = self;
    interface.didFinished = @selector(getPhotoSettingFinished:);
    interface.didFinishedError = @selector(getPhotoSettingFinishedError);
    [interface getSetting:PHOTOTAB];
}

- (void)getPhotoSettingFinished:(id)data
{
    //burst mode
    NSData *burstModeData = [self getSubData:data RangStartStr:@"<p>\"burst mode\"</p>" RangEndStr:@"<p>\"size\"</p>"];
    TFHpple *burstModeXpathParser = [[TFHpple alloc] initWithHTMLData:burstModeData];
    
    NSArray *burstModeElements = [burstModeXpathParser searchWithXPathQuery:@"//option"];
    
    if (burstModeElements.count) {
        
        [self.burstModeArr removeAllObjects];
        
        for (int i = 0;i < burstModeElements.count;i++) {
            
            TFHppleElement *element = [burstModeElements objectAtIndex:i];
            
            NSString *value = [[[element firstChild] content] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            if (i == 0) {
                
                self.burstMode = value;
            }else{
                
                if (value) {
                    [self.burstModeArr addObject:value];
                }
            }
        }
    }
    
    //photo size
    NSData *photoSizeData = [self getSubData:data RangStartStr:@"<p>\"size\"</p>" RangEndStr:@"<p>\"photo quality\"</p>"];
    TFHpple *photoSizeXpathParser = [[TFHpple alloc] initWithHTMLData:photoSizeData];
    
    NSArray *photoSizeElements = [photoSizeXpathParser searchWithXPathQuery:@"//option"];
    
    if (photoSizeElements.count) {
        
        [self.photoSizeArr removeAllObjects];
        
        for (int i = 0;i < photoSizeElements.count;i++) {
            
            TFHppleElement *element = [photoSizeElements objectAtIndex:i];
            
            NSString *value = [[[element firstChild] content] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            if (i == 0) {
                
                self.photoSize = value;
            }else{
                
                if (value) {
                    [self.photoSizeArr addObject:value];
                }
            }
        }
    }
    
    //Photo Stamp
    NSData *photoStampData = [self getSubData:data RangStartStr:@"<p>\"photo stamp\"</p>" RangEndStr:@"</body>"];
    TFHpple *photoStampXpathParser = [[TFHpple alloc] initWithHTMLData:photoStampData];
    
    NSArray *photoStampElements = [photoStampXpathParser searchWithXPathQuery:@"//option"];
    
    if (photoStampElements.count) {
        
        [self.photoStampArr removeAllObjects];
        
        for (int i = 0;i < photoStampElements.count;i++) {
            
            TFHppleElement *element = [photoStampElements objectAtIndex:i];
            
            NSString *value = [[[element firstChild] content] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            if (i == 0) {
                
                self.photoStamp = value;
            }else{
                
                if (value) {
                    [self.photoStampArr addObject:value];
                }
            }
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(getConfigFinished)]) {
        [self.delegate getConfigFinished];
    }
}

- (void)getPhotoSettingFinishedError
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(getConfigFinishedError)]) {
        [self.delegate getConfigFinishedError];
    }
}

- (NSString *)videoResolution
{
    if (_videoResolution == nil) {
        _videoResolution = [[NSString alloc] init];
    }
    
    return _videoResolution;
}

- (NSMutableArray *)videoResolutionArr
{
    if (_videoResolutionArr == nil) {
        _videoResolutionArr = [[NSMutableArray alloc] init];
    }
    
    return _videoResolutionArr;
}

- (NSString *)dualStreams
{
    if (_dualStreams == nil) {
        _dualStreams = [[NSString alloc] init];
    }
    
    return _dualStreams;
}

- (NSMutableArray *)dualStreamsArr
{
    if (_dualStreamsArr == nil) {
        _dualStreamsArr = [[NSMutableArray alloc] init];
    }
    
    return _dualStreamsArr;
}

- (NSString *)videoStamp
{
    if (_videoStamp == nil) {
        _videoStamp = [[NSString alloc] init];
    }
    
    return _videoStamp;
}

- (NSMutableArray *)videoStampArr
{
    if (_videoStampArr == nil) {
        _videoStampArr = [[NSMutableArray alloc] init];
    }
    
    return _videoStampArr;
}

- (NSString *)burstMode
{
    if (_burstMode == nil) {
        _burstMode = [[NSString alloc] init];
    }
    
    return _burstMode;
}

- (NSMutableArray *)burstModeArr
{
    if (_burstModeArr == nil) {
        _burstModeArr = [[NSMutableArray alloc] init];
    }
    
    return _burstModeArr;
}

- (NSString *)photoSize
{
    if (_photoSize == nil) {
        _photoSize = [[NSString alloc] init];
    }
    
    return _photoSize;
}

- (NSMutableArray *)photoSizeArr
{
    if (_photoSizeArr == nil) {
        _photoSizeArr = [[NSMutableArray alloc] init];
    }
    
    return _photoSizeArr;
}

- (NSString *)photoStamp
{
    if (_photoStamp == nil) {
        _photoStamp = [[NSString alloc] init];
    }
    
    return _photoStamp;
}

- (NSMutableArray *)photoStampArr
{
    if (_photoStampArr == nil) {
        _photoStampArr = [[NSMutableArray alloc] init];
    }
    
    return _photoStampArr;
}

#pragma mark - 截取html字符

- (NSData *)getSubData:(NSData *)data RangStartStr:(NSString *)rangeStartStr RangEndStr:(NSString *)rangEndStr
{
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSRange rangStart = [string rangeOfString:rangeStartStr];
    
    if (rangStart.location > string.length) {
        
        return [string dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    string = [string substringFromIndex:rangStart.location+rangStart.length];
    NSRange rangEnd = [string rangeOfString:rangEndStr];
    
    if (rangEnd.location > string.length) {
        
        return [string dataUsingEncoding:NSUTF8StringEncoding];
    }
    string = [string substringToIndex:rangEnd.location];
    
    NSData *htmlData = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    return htmlData;
}

@end
