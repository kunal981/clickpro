//
//  main.m
//  Camera
//
//  Created by Apple_ZJ on 13-10-22.
//  Copyright (c) 2013年 ifoer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZJAppDelegate class]));
    }
}
